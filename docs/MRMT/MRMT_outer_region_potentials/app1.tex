% ********** Appendix 1 **********
\subsection{$W^{E}$ potentials for electron-target interaction}\label{sec:appendix1}

In this section we derive the expression for the potentials $W^{E}$ which describe field-free interaction of the electron with the residual target (typically ion). These potentials arise from the term $H_{N+1}$ in equation (\ref{we}) and their final form is:
\begin{equation}
W_{ij}^{E}(r)=\sum_{\lambda = 0}^{\infty}a_{ij\lambda}r^{-\lambda-1}, \mbox{ $i,j=1,\dots,n$, $r \geq a$.}\label{ap1:eq:couplings}
\end{equation}
The potentials are defined as the matrix elements:
\begin{eqnarray}
 W_{ij}^{E}(r) = \bigg(\chnl{i}{i}\bigg\vert \sum_{p=1}^{N}\frac{1}{r_{p(N+1)}}-\sum_{k=1}^{Nuclei}\frac{Z_{k}}{\rho_{k(N+1)}}\bigg\vert \chnl{j}{j}\bigg).\label{ap1:eq:Vij}
\end{eqnarray}
The round brackets indicate integration over all spin-space coordintes with exception of the radial coordinate of the $(N+1)$th electron, $r_{p(N+1)}$ is the distance between the p-th and the (N+1)th electron and $\rho_{k(N+1)}$ is the distance between the k-th nucleus and the (N+1)th electron. The derivation of $W_{ij}^{E}(r)$ is based on the expansion of the Coulomb potential in Legendre polynomials: %\cite{friedrich2006}:
\begin{equation}
\frac{1}{r_{ij}}=\frac{1}{\vert \mathbf{r_i}-\mathbf{r_j}\vert}=\sum_{\lambda=0}^{\infty} \frac{{r_j}^{\lambda}}{r_{i}^{\lambda+1}}P_{\lambda}(\cos\theta),\mbox{ $r_{j}\leq r_{i}$,} \label{ap1:eq:leg}
\end{equation}
where $\theta$ is the angle between the vectors $\mathbf{r_i}$ and $\mathbf{r_j}$, ($\cos\theta=\hat{\mathbf{r}}_{i}.\hat{\mathbf{r}}_{j}$). In the outer region the distance of the scattering electron from the origin is always greater than that of the target molecules' electrons: $r_{N+1}>r_{i}$, $i=1,\dots,N$ and the molecule's nuclei. Therefore in the expressions below, the radial distance of the scattering electron will always be in the denominator.

Using the Legendre expansion (\ref{ap1:eq:leg}), we can calculate the contribution to the potentials $W_{ij}^{E}(r)$ of the first term in (\ref{ap1:eq:Vij}):
\begin{eqnarray}
&&\bigg(\chnl{i}{i}\bigg\vert \sum_{p=1}^{N}\frac{1}{r_{p(N+1)}}\bigg\vert\chnl{j}{j}\bigg)=\\ &&=\sum_{\lambda=0}^{\infty}\frac{1}{r^{\lambda+1}}\sum_{p=1}^{N}\left(\tgt{t_{i}}\rsh{i}\big\vert P_{\lambda}(\hat{\mathbf{x}}_{N+1}.\hat{\mathbf{x}}_{p})r_{p}^\lambda\big\vert\tgt{t_{j}}\rsh{j}\right).
\end{eqnarray}
Utilizing the addition theorem of real spherical harmonics:
\begin{eqnarray}
P_{l}(\hat{\mathbf{x}}_{i}.\hat{\mathbf{x}}_{j})=\frac{4\pi}{2l+1}\sum_{m=-l}^{l}{\cal {Y}}_{lm}(\hat{\mathbf{x}}_{i}){\cal {Y}}_{lm}(\hat{\mathbf{x}}_{j}),
\end{eqnarray}
we can carry out separately, in the preceding equation, the integrations over the coordinates of the target molecule's electrons and the angular coordinates of the scattering electron and obtain:
\begin{eqnarray}
&&\sum_{\lambda=0}^{\infty}\frac{1}{r^{\lambda+1}}\sum_{p=1}^{N}\left(\chnl{i}{i}\big\vert P_{\lambda}(\hat{\mathbf{x}}_{N+1}.\hat{\mathbf{x}}_{p})r_{p}^\lambda\big\vert\chnl{j}{j}\right)=\\
&=&\delta_{\Gamma^{i},\Gamma^{j}}\sum_{\lambda=0}^{\infty}\sum_{m=-\lambda}^{\lambda}\frac{4\pi}{2\lambda+1}\frac{T_{t_{i}t_{j}}^{\lambda m}}{r^{\lambda+1}}\langle{\cal Y}_{l_{i},m_{i}}\vert {\cal {Y}}_{\lambda ,m}\vert{\cal Y}_{l_{j},m_{j}}\rangle,\label{ap1:eq:term1}
\end{eqnarray}
where the term $\delta_{\Gamma^{i},\Gamma^{j}}$ arises since wavefunctions of different total symmetries are not coupled by the field-free Hamiltonian and the elements of the matrix $\mathbf{T}^{\lambda m}$ are given by the formula:
\begin{eqnarray}
T_{t_{i}t_{j}}^{\lambda m}=\sum_{p=1}^{N}\langle\Phi_{t_{i}}\vert {\cal {Y}}_{\lambda ,m}(\hat{\mathbf{x}}_{p})r_{p}^\lambda\vert\Phi_{t_{j}}\rangle.
\end{eqnarray}
The second term in (\ref{ap1:eq:Vij}) is calculated similarly:
\begin{eqnarray}
&&\bigg(\chnl{i}{i}\bigg\vert -\sum_{k=1}^{Nuclei}\frac{Z_{k}}{\rho_{k(N+1)}}\bigg\vert\chnl{j}{j}\bigg)=\nonumber\\
&=&-\sum_{\lambda=0}^{\infty}\frac{1}{r^{\lambda+1}}\sum_{k=1}^{Nuclei}\bigg(\chnl{i}{i}\bigg\vert Z_{k}P_{\lambda}(\hat{\mathbf{x}}_{N+1}.\hat{\mathbf{R}}_{k})R_{k}^{\lambda}\bigg\vert\chnl{j}{j}\bigg)=\nonumber\\
&=&-\delta_{\Gamma^{i},\Gamma^{j}}\langle \Phi_{t_{i}}\vert\Phi_{t_{j}}\rangle \sum_{\lambda=0}^{\infty}\sum_{m=-\lambda}^{\lambda}\frac{4\pi}{2\lambda+1}\frac{1}{r^{\lambda+1}}\langle{\cal Y}_{l_{i},m_{i}}\vert {\cal {Y}}_{\lambda ,m}\vert{\cal Y}_{l_{j},m_{j}}\rangle\sum_{k=1}^{Nuclei}Z_{k}{\cal Y}_{\lambda,m}(\hat{\mathbf{R}}_{k})R_{k}^{\lambda}.\label{ap1:eq:term2}
\end{eqnarray}
Collecting the two terms which we just evaluated (equations (\ref{ap1:eq:term1}) and (\ref{ap1:eq:term2})) we get the final expression for the coupling potentials $W_{ij}^{E}(r)$:
\begin{eqnarray}
&&W_{ij}^{E}(r)=\delta_{\Gamma^{i},\Gamma^{j}}\sum_{\lambda=0}^{\infty}\frac{1}{r^{\lambda+1}}\sum_{m=-\lambda}^{\lambda}\langle{\cal Y}_{l_{i},m_{i}}\vert {\cal {Y}}_{\lambda ,m}\vert{\cal Y}_{l_{j},m_{j}}\rangle\times\nonumber\\ 
&\times&\underbrace{\sqrt{\frac{4\pi}{2\lambda+1}} \underbrace{\sqrt{\frac{4\pi}{2\lambda+1}}\left( T_{t_{i}t_{j}}^{\lambda m} - \langle \Phi_{t_{i}}\vert\Phi_{t_{j}}\rangle \sum_{k=1}^{Nuclei}Z_{k}{\cal Y}_{\lambda,m}(\hat{\mathbf{R}}_{k})R_{k}^{\lambda}\right)}_{Q_{t_{i}t_{j}}^{\lambda m}}}_{a_{ij\lambda}},\label{ap1:eq:final}
\end{eqnarray}
which conicides with (\ref{ap1:eq:couplings}). The coefficients $Q_{t_{i}t_{j}}^{\lambda m}$ are the spherical multipole permanent ($t_{i}=t_{j}$) and transition ($t_{i}\neq t_{j}$) moments of the electronic states of the target molecule. Finally, the term $\langle{\cal Y}_{l_{i},m_{i}}\vert {\cal {Y}}_{\lambda ,m}\vert{\cal Y}_{l_{j},m_{j}}\rangle$ is an integral over three real spherical harmonics, which can be calculated using the formula:%~\cite{homeier1996}:
\begin{equation}\label{rg}
\langle{\cal Y}_{l_{i},m_{i}}\vert {\cal {Y}}_{\lambda ,m}\vert{\cal Y}_{l_{j},m_{j}}\rangle=\sum_{m_{2},m_{3}}\left[U_{l_{i},m_{2}+m_{3}}^{m_{i}}\right]^{*}U_{\lambda,m_{2}}^{m}U_{l_{j},m_{3}}^{m_{j}}\left( l_{i},m_{2}+m_{3},\lambda ,m_{2}\vert l_{j},m_{4} \right),
\end{equation}
where the last term on the right hand side is the usual Clebsch-Gordan coefficient. The unitary matrix $\mathbf{U}$ provides the mapping between the complex and the real spherical harmonics and its form is:
\begin{eqnarray}
U_{lm}^{\mu}=\delta_{m0}\delta_{\mu 0}&+&\frac{1}{\sqrt{2}}\left[ \theta(\mu)\delta_{m\mu}+\theta(-\mu)(+\imath)(-1)^{m}\delta_{m\mu}+\theta(-\mu)(-\imath)\delta_{m -\mu}+\right.
\\&+&\left. \theta(\mu)(-1)^{m}\delta_{m-\mu} \right] ,\nonumber
\end{eqnarray}
with
\begin{eqnarray}
\theta(m)=
\begin{cases}
1 & \mbox{, for $m>0$}, \\
0 & \mbox{, for $m\leq 0$}.
\end{cases}
\end{eqnarray}

It can be shown that the coefficient (\ref{rg}) is always proportional to a single standard Gaunt coefficient (integral over three complex spherical harmonics) or zero. Additionally, the coefficients (\ref{rg}) are invariant with respect to any permutation of the three pairs of $\{l,m\}$ values.

%The sum over $\lambda$ in (\ref{ap1:eq:final}) formally extends to infinity, but in practice this sum is always finite, because of the finite number of the spherical harmonics included in the calculation and the triangular inequality for addition of the spherical harmonics:
%\begin{equation}
%\lambda = l_{i}+l_{j},l_{i}+l_{j}-1,\dots,\vert l_{i}-l_{j}\vert.
%\end{equation}
%Let $l_{M}$ be the largest angular momentum of the unbound electron included in the calculation. We can see that the coupling coefficients $a_{ij\lambda}$ which can be nonzero correspond only to the following values of $\lambda$:
%\begin{equation}
%\lambda=0,1,\dots,2l_{M}. 
%\end{equation}

% ********** End of appendix **********
