"""
Utility for generating harmonic spectra from RMT output. The utility should be
invoked from the command line as

>>> python3 ./gen_hhg.py [-xyz] [-h] [--mask] <filelist> 

Options
=======
-xyz    :  which components of the dipole to transform. DEFAULT: compute z only
-h      :  print a help message
--mask  :  attempt to mask out the long trajectory contribution. DEFAULT off

Arguments
=========

<filelist> :  is a list of directories containing the RMT output.

specifically the expec_z_all.* and/or expec_v_all* files are required.
Additionally, if the masking utility is desired the EField* file is required.

Can be invoked for the current directory using

>>> python3 ./gen_hhg.py [-xyz] [-h] [--mask] .

The result will be new files in the RMT directory: [Harm,Smooth]_[len,vel][x,y,z]

which contain the harmonic spectra as calculated from the dipole length/velocity
and the corresponding smoothed spectra for the requested caretsian components.

If invoked with the --mask option, the code also produces
harmonic spectra as calculated with a masked version of the dipole
length/velocity which attempts to eliminate the contribution of the long trajectories (see
note below). 

If either the expec_z_all.* or expec_v_all.* RMT output files are missing, then
the corresponding spectra will not be computed. If both are missing, or if any
errors are encountered, the code will print an error message.

Note that there is an option to prepend a header to the output files to make
them look good when plotted (e.g. with xmgrace). The one I use is located in the
repository under "/Molecular_RMT/utilities/grace_header.agr", and is using the
environment variable RMT_REPO. I.E, in order to use this header file, you should
add 

>>> export RMT_REPO=<path_to_RMT_repository> 

to your .bashrc. Or to use a custom header, you can simply edit the line below
in the clearly labelled section. By default (I.E unless you set the environment
variable RMT_REPO) there is no header- the code simply writes the transformed data to file.

ACB 20/03/2019

Note on *mask files.
===================

Please note that the windowing of the time dependent dipole eliminates the
contribution of the long trajectories, but also suppresses the highest energy
short trajectories. Thus this analysis is useful for investigating low energy
and plateau harmonics, but will be unreliable for probing the cutoff. This
function will also be inaccurate for two colour fields (2 IR pulses) as the
field zeros will not correspond exactly (necessarily) with the interface of long
and short trajectories.

"""
##################################################
from __future__ import print_function
import os
from multiprocessing import Pool, cpu_count
import filehand as fh
import datahand as dh

##################################################
# If you want to set a custom header file, do so here

repo_dir=os.getenv("RMT_REPO",None)
if repo_dir == None:
    header_file = "" # change this if you wish to use a custom header file
else:
    header_file= repo_dir+"/utilities/grace_header.agr"

##################################################

def get_header(header_file):
    """ read the header data from the given file"""
    if os.path.isfile(header_file):
        with open(header_file,'r') as headf:
            gr_header = headf.read()
        return gr_header
    elif header_file=="":
        return ""
    else:
        print("header_file not found")
        return ""

def do_conversion(dir_name): # LUKE - duplicated components in ellipticity; check / generalise
    """
    attempts to peform the calculation of the harmonic spectra for a given
    directory. If this fails, will exit gracefully and print an error message
    to screen.
    """
# get which components of dipole are desired
    #components, mask = fh.get_options()
    options = fh.get_options()

    solutions_chosen_is_range = True if options['solutions_end'] is not None else False
    if solutions_chosen_is_range:
        solutions = range(options['solutions_start'],options['solutions_end']+1)
    else:
        solutions = range(options['solutions_start'],options['solutions_start']+1)

    try:
        os.chdir(dir_name)

        for sol_id in solutions:

            for name,gauge in zip(['len','vel'],["z","v"]):
                for desired,dim_name,dim_id in zip(options["componentlist"],['x','y','z'],[1,2,3]):

                    if desired: # if we have been asked to compute this component
                        try:
                            header=get_header(header_file)
                            if options["setfieldz"]:
                                field_id = 3
                            else:
                                field_id = dim_id
                        
    # First calculate the standard HHG spectrum
                            dt,data,field = fh.get_data_and_field(gauge, dim_id, field_id, sol_id)
                            freqs,harm_data,smoothed = dh.get_hhg_spect(dt, data, gauge, pad_factor=options['pad_factor'])
                            fh.write_xy_output(freqs,harm_data,'Harm_'+name+'_{0:04d}_'.format(sol_id)+dim_name,filehead=header)
                            fh.write_xy_output(freqs,smoothed,'Smooth_'+name+'_{0:04d}_'.format(sol_id)+dim_name,filehead=header)


    #       Now apply the mask and convert again
    #       If the EField file is present, read the data and construct the gaussian
    #       mask function which will screen out the long-trajectory harmonics
                            if options["mask"]:
                                try:
                                    data_masked_short = dh.mask_data(data,field)
                                    freqs,harm_data,smoothed = dh.get_hhg_spect(dt, data_masked_short, gauge, pad_factor=options['pad_factor'])
                                    fh.write_xy_output(freqs,harm_data,'Harm_'+name+'_{0:04d}_'.format(sol_id)+dim_name+"_mask",filehead=header)
                                    fh.write_xy_output(freqs,smoothed,'Smooth_'+name+'_{0:04d}_'.format(sol_id)+dim_name+"_mask",filehead=header)
                                except Exception as e:
                                    print("correct column of field file must be present to use short mask function in",dim_name,"dimension")
                                    raise e

    #       Now apply the mask and convert again
    #       If the EField file is present, read the data and construct the gaussian
    #       mask function which will screen out the long-trajectory harmonics
                            if options["masklong"]:
                                try:
                                    data_masked_long = dh.mask_datalong(data,field)
                                    freqs,harm_data,smoothed = dh.get_hhg_spect(dt, data_masked_long, gauge, pad_factor=options['pad_factor'])
                                    fh.write_xy_output(freqs,harm_data,'Harm_'+name+'_{0:04d}_'.format(sol_id)+dim_name+"_mask_long",filehead=header)
                                    fh.write_xy_output(freqs,smoothed,'Smooth_'+name+'_{0:04d}_'.format(sol_id)+dim_name+"_mask_long",filehead=header)
                                except Exception as e:
                                    print("correct column of field file must be present to use long mask function in",dim_name,"dimension")
                                    raise e
                                    
                            if options["writemask"]:
                                try:
                                    fh.write_mask_file(gauge,dim_id,field_id,sol_id,'mask'+name+dim_name)
                                except Exception as e:
                                    print("correct column of field file must be present to write mask functions in",dim_name,"dimension")
                                    raise e

                        except Exception as e:
                            print('tried to operate on {}{} dimension {}, but failed with the following error. (Processing continuing)'.format(name,gauge,dim_name), e)
                            pass

    except Exception as e:
        print("could not convert {}. failed with error message:\n".format(dir_name),e)
        pass
################################################################################


# get list of directories
filelist = fh.get_filelist()


# use the p.map parallelisation to convert the listed directories in parallel.
p = Pool(cpu_count())
p.map(do_conversion,filelist)
