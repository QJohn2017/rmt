from __future__ import print_function
import os
from multiprocessing import Pool, cpu_count
import filehand as fh
import datahand as dh
import numpy as np

def get_hhg_spect(dt,data,gauge):
    """
    Given the raw input from RMT, calculate the harmonic spectrum and the
    smoothed harmonic spectrum in the given dipole form (gauge)
    """
    freqs = dh.get_frequency_axis(dt,data)
#    data=dh.apply_blackman(data)
    data = dh.FFT(data)
    harm_data = abs(data)*abs(data)
    harm_data = rescale_data(gauge,freqs,harm_data)
    freqs = 27.212*freqs # rescales to eV
    smoothed = dh.smooth_data(harm_data)

    return freqs,harm_data,smoothed
    
def rescale_data(gauge,x,y): # LUKE - duplicated in datahand; check / generalise
    """
    Scale the power spectrum by the appropriate power of the frequency 
    """
# Note that the factor of 0.67 here is simply for consistency with old
# scripts for calculating the HHG spectrum. The units are arbitrary so it
# doesn't matter, but it can be removed if desired.
    if ( gauge == "z" ):
        return(0.67*1e-5*y*(x**4))
    elif (gauge == "v"):
        return(0.67*1e-5*y*(x*x))
    else:
        raise ValueError('gauge should be one of "z" or "v"')

def do_tfa(dir_name): # LUKE - components duplicated in datahand; check / generalise
    """
    attempts to peform the calculation of the harmonic spectra for a given
    directory. If this fails, will exit gracefully and print an error message
    to screen.
    """
# get which components of dipole are desired

    N_Times=200
    Window_Width=100
    dPoints=750

    options = fh.get_options()

    solutions_chosen_is_range = True if options['solutions_end'] is not None else False
    if solutions_chosen_is_range:
        solutions = range(options['solutions_start'],options['solutions_end']+1)
    else:
        solutions = range(options['solutions_start'],options['solutions_start']+1)

    #print(dir_name)
    try:
        os.chdir(dir_name)
        for sol_id in solutions:
            for name,gauge in zip(['len','vel'],["z","v"]):
                for desired,dim_name,dim_id in zip(options["componentlist"],['x','y','z'],[1,2,3]):

                    if desired: # if we have been asked to compute this component
                        try:
                            input_file_name=fh.get_fname('expec',gauge)
                            dt,data=fh.get_raw_data(input_file_name, sol_id, dim_id)


                            freqs,windowed_harm_data,windowed_smoothed=get_hhg_spect(dt,data,gauge)
                            d_times=np.float(dt*len(data))/float(N_Times)
                            times=np.linspace(0,dt*len(data),N_Times)
                            print (N_Times,len(data))
                            allharm=np.zeros((N_Times,dPoints))
                            allsmoothed=np.zeros((N_Times,dPoints))
                            windowed_data=np.zeros(len(data))
                            windowed_gauss=np.zeros(len(data))
                            
                            print('Number of time points: ',len(data))
                            print('Final_time: ',dt*len(data))
                            print('Delta time: ',d_times)
                                    
                            for time_id in range(0,N_Times):
                                current_time=d_times*time_id
                                print(time_id,current_time)
                                for gauss_point_id in range(0,len(data)):
                                    gauss_point=dt*gauss_point_id
                                    current_window_value=np.exp(-(gauss_point-current_time)**2/(Window_Width*Window_Width))
                                    windowed_data[gauss_point_id]=current_window_value*data[gauss_point_id]
                                    windowed_gauss[gauss_point_id]=current_window_value

                                freqs,windowed_harm_data,windowed_smoothed=get_hhg_spect(dt,windowed_data,gauge)
                                allharm[time_id,:]=windowed_harm_data[0:dPoints]
                                allsmoothed[time_id,:]=windowed_smoothed[0:dPoints]
                                    
                            header=''
                            fh.write_xyz_output(times,freqs[0:dPoints],allharm[:,0:dPoints],'tfaHarm_'+name+'_{0:04d}_'.format(sol_id)+dim_name,filehead=header)
                            fh.write_xyz_output(times,freqs[0:dPoints],allsmoothed[:,0:dPoints],'tfaSmooth_'+name+'_{0:04d}_'.format(sol_id)+dim_name,filehead=header)
                        except Exception as e:
                            print('Failed to operate on {}{} dimension {}, solution {}. Failed with the following error (Process continuing)\n'.format(name,gauge,dim_name,sol_id),e)
                            pass
    except Exception as e:
        print('unable to change directory to {}'.format(dir_name))
        raise e

################################################################################

# get list of directories
filelist = fh.get_filelist()

# use the p.map parallelisation to convert the listed directories in parallel.
p = Pool(cpu_count())
do_tfa(filelist[0])
#p.map(do_tfa,filelist)
