
"""
Utility for generating the harmonic phase from RMT output. The utility should be
invoked from the command line as

>>> python3 ./harm_phase.py [-xyz] [-h] <filelist> 

Options
=======
-xyz    :  which components of the dipole to transform. DEFAULT: compute z only
-h      :  print a help message

Arguments
=========

<filelist> :  is a list of directories containing the RMT output.

specifically the expec_z_all.* and/or expec_v_all* files are required.

Can be invoked for the current directory using

>>> python3 ./harm_phase.py [-xyz] [-h] .

The result will be new files in the RMT directory named Phase_<gauge>_<component>
where <gauge> can be either "len" or "vel" (calculated from dipole
length/velocity). Component will be x y or z as requested.

If either the expec_z_all.* or expec_v_all.* RMT output files are missing, then
the corresponding phase will not be computed. If both are missing, or if any
errors are encountered, the code will print an error message.

ACB 20/03/2019
"""
########################################
from __future__ import print_function
import os
from multiprocessing import Pool, cpu_count
import filehand as fh
import datahand as dh
import numpy as np


def undo_phase_shift(freqs,phase):
    phase_shift=0
    new_phase=np.zeros(len(phase))
    for counter in range(1,len(freqs)):
        if phase[counter]<(-2*np.pi/8):
            if phase[counter-1]>(2*np.pi/8):
                if phase[counter-1]>phase[counter-2]:
                    phase_shift=phase_shift+1

        if phase[counter]>(2*np.pi/8):
            if phase[counter-1]<(-2*np.pi/8):
                if phase[counter-1]<phase[counter-2]:
                    phase_shift=phase_shift-1

        new_phase[counter]=phase[counter]+phase_shift*np.pi
    return new_phase

def write_phase_to_files(options,freqs,phase,longsuffix,sol_id):
    fh.write_xy_output(freqs[1:],phase,'Phase'+'_{0:04d}'.format(sol_id)+longsuffix)
                            
    if options["continuousphase"]:
        new_phase=undo_phase_shift(freqs[1:],phase)
        fh.write_xy_output(freqs[1:],new_phase,'CPhase'+'_{0:04d}'.format(sol_id)+longsuffix)



def do_conversion(dirname): # LUKE - duplicated in a few files; check / generalise
    """
    attempts to peform the calculation of the fourier transform for the dipole
    expectation files in a given directory. If this fails, will exit gracefully
    and print an error message to screen.
    """
# get which components of dipole are desired
    options = fh.get_options()

    solutions_chosen_is_range = True if options['solutions_end'] is not None else False
    if solutions_chosen_is_range:
        solutions = range(options['solutions_start'],options['solutions_end']+1)
    else:
        solutions = range(options['solutions_start'],options['solutions_start']+1)

    try:
        os.chdir(dirname)
        for sol_id in solutions:
    #       If the EField file is present, read the data and construct the gaussian
    #       mask function which will screen out the long-trajectory harmonics

            for gauge,suffix in zip(["z","v"],["_len","_vel"]):
                for desired,dim_name,dim_id in zip(options["componentlist"],['x','y','z'],[1,2,3]):
                    try:
                    
                        if desired :
                            #Initialise field
                            if options["setfieldz"]:
                                field_id=3
                            else:
                                field_id=dim_id
                        
                            #Calculate unmasked phase
                            dt,data,field=fh.get_data_and_field(gauge, dim_id, field_id, sol_id)
                            freqs,phase=dh.make_phase_data(data,gauge,dt, pad_factor=options['pad_factor'])
                            if options["filterharmonicpeaks"]:
                                phase=dh.filterbyomega(freqs,phase,options["omega"])
                            write_phase_to_files(options,freqs,phase,suffix+dim_name, sol_id)
        
                            #Calculate phase masked for short trajectories
                            if options["mask"]:
                                try:
                                    dt,data,field=fh.get_data_and_field(gauge, dim_id, field_id, sol_id)
                                    data=dh.mask_data(data,field)
                                    freqs,phase=dh.make_phase_data(data,gauge,dt, pad_factor=options['pad_factor'])
                                    if options["filterharmonicpeaks"]:
                                        phase=dh.filterbyomega(freqs,phase,options["omega"])
                                    write_phase_to_files(options,freqs,phase,suffix+dim_name+'_mask', sol_id)

                                except:
                                    print("correct column of field file must be present to use short mask function in configuration",sol_id,"in",dim_name,"dimension")
                                    pass
                                    
                            #Calculate phase masked for long and cuttoff trajectories
                            if options["masklong"]:
                                try:
                                    dt,data,field=fh.get_data_and_field(gauge, dim_id, field_id, sol_id)
                                    data=dh.mask_datalong(data,field)
                                    freqs,phase=dh.make_phase_data(data,gauge,dt, pad_factor=options['pad_factor'])
                                    if options["filterharmonicpeaks"]:
                                        phase=dh.filterbyomega(freqs,phase,options["omega"])
                                    write_phase_to_files(options,freqs,phase,suffix+dim_name+'_masklong', sol_id)

                                except:
                                    print("correct column of field file must be present to use long mask function in configuration",sol_id,"in",dim_name,"dimension")
                                    pass
                
                            #Write out the mask files
                            if options["writemask"]:
                                try:
                                    fh.write_mask_file(gauge,dim_id,field_id,sol_id,'mask'+suffix+dim_name)
                                except:
                                    print("correct column of field file must be present to write mask functions in configuration",sol_id,"in",dim_name,"dimension")
                                    pass
                            
                    except Exception as e:
                        print('failed to process {}{}, dimension {} solution {}. Error messaage shown below (Process Continuing)'.format(gauge,suffix,dim_name,sol_id),e)
                        pass
    except Exception as e:
        print("A problem occured trying to change to directory {}".format(dirname))
        raise e
################################################################################

filelist = fh.get_filelist()
# use the p.map parallelisation to convert the listed directories in parallel.
p = Pool(cpu_count())
p.map(do_conversion,filelist)
