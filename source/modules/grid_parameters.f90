! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up outer region grid and communications associated therewith.

MODULE grid_parameters

    USE precisn, ONLY: wp
    
    IMPLICIT NONE

    ! Each outer region processor has psi_outer on grid points X_1st to X_Last
    ! The 1st PE in the outer region has a smaller value of X_Last than the others
    ! because this PE has additional work to do to get H^n values at the two grid
    ! points inside the inner region
    !
    ! HUGO: All integrations should use a 5-point method similar to the determination
    !       of derivatives. The total number of integration points should therefore be
    !       a multiple of 4 plus 1. The easiest way to implement this is by a grid length
    !       of 4n+1 for the Outer Master node (the 1st PE in the outer region), and a grid
    !       length of 4m for every other. This allows us to define the weights for the
    !       self_local_inner, local_inner and real_local_inner in a straightforward manner.
    
    INTEGER, PARAMETER  :: x_1st = 1
    INTEGER, SAVE       :: x_last

    ! Each outer region processor handles a number of grid points given by Block_Length:
    INTEGER             :: block_length_master != x_last_master - x_1st + 1
    INTEGER             :: block_length_others != x_last_others - x_1st + 1

    ! R_1st to R_Last is total number of grid points in the outer region
    ! The outer region is made up of no_of_blocks, each with Block_Length grid points
    INTEGER, PARAMETER  :: r_1st = 1
    INTEGER             :: r_last  !=  block_length_master  +                      &
!                                      ((no_of_blocks-1) * block_length_others) +  &
!                                      r_1st - 1

    INTEGER, SAVE       :: no_of_states_in_angular_basis
    ! DO NOT CHANGE
    INTEGER, PARAMETER  :: channel_id_1st = 1 ! MUST = 1
    INTEGER, SAVE       :: channel_id_last

    !  Parameters for separating grid into ionization regions:
!   REAL(wp),PARAMETER  :: single_ioniz_bndry_in_au = 50.0_wp
!   INTEGER,SAVE        :: single_ioniz_bndry_in_grid_pts

CONTAINS

    SUBROUTINE init_channel_id_last(number_channels)
        USE initial_conditions, ONLY : debug

        INTEGER, INTENT(IN) :: number_channels

        channel_id_last = number_channels + channel_id_1st - 1


        IF (debug) PRINT *, 'Channel_ID_Last = ', channel_id_last

    END SUBROUTINE init_channel_id_last

!-----------------------------------------------------------------------

    SUBROUTINE init_grid_parameters_module(i_am_outer_master, &
                                           half_fd_order, &
                                           propagation_order, &
                                           i_am_in_outer_region)

        USE calculation_parameters, ONLY: init_nfdm

        LOGICAL, INTENT(IN) :: i_am_outer_master
        LOGICAL, INTENT(IN) :: i_am_in_outer_region
        INTEGER, INTENT(IN) :: half_fd_order
        INTEGER, INTENT(IN) :: propagation_order

        CALL init_grid_parameters(i_am_outer_master)

        CALL check_propagation_order(half_fd_order, propagation_order, &
                                     i_am_in_outer_region)

        CALL init_nfdm

    END SUBROUTINE init_grid_parameters_module

!-----------------------------------------------------------------------

    SUBROUTINE init_grid_parameters(i_am_outer_master)

        USE initial_conditions, ONLY: x_last_master, x_last_others

        LOGICAL, INTENT(IN) :: i_am_outer_master

        IF (i_am_outer_master) THEN
            x_last = x_last_master
        ELSE
            x_last = x_last_others
        END IF

    END SUBROUTINE init_grid_parameters

!-----------------------------------------------------------------------

    SUBROUTINE check_propagation_order(half_fd_order, propagation_order, &
                                       i_am_in_outer_region)

        USE rmt_assert, ONLY: assert

        INTEGER, INTENT(IN) :: half_fd_order
        INTEGER, INTENT(IN) :: propagation_order
        LOGICAL, INTENT(IN) :: i_am_in_outer_region

        IF (i_am_in_outer_region) THEN
            CALL assert(MOD(x_last, 2*half_fd_order) .EQ. 0, 'Both X_Last parameters should be divisible by 4')
        END IF

        CALL assert(half_fd_order .EQ. 2, 'only five-point methods implemented')

        IF (x_last .LT. half_fd_order*propagation_order) THEN
            PRINT *, ' X_Last=', x_last
            PRINT *, ' half_fd_order=', half_fd_order
            PRINT *, ' Propagation_Order=', propagation_order
            CALL assert(.false., 'Error: X_Last needs to be .GE. half_fd_order*Propagation_Order')
        END IF

    END SUBROUTINE check_propagation_order

!-----------------------------------------------------------------------

    SUBROUTINE derive_grid_parameters

        USE communications_parameters, ONLY: no_of_blocks
        USE initial_conditions,        ONLY: x_last_master, x_last_others

        block_length_master = x_last_master - x_1st + 1
        block_length_others = x_last_others - x_1st + 1

        ! r_1st to r_Last is total number of grid points in the outer region
        ! The outer region is made up of no_of_blocks, each with Block_Length grid points
!       INTEGER, PARAMETER     :: r_1st   = 1
        r_last = block_length_master + &
                 ((no_of_blocks - 1)*block_length_others) + &
                 r_1st - 1

    END SUBROUTINE derive_grid_parameters

END MODULE grid_parameters


!> @page outer_para Outer Region Parallelisation
!!
!! @brief Outer Region Parallelisation Strategy
!!
!!
!!### Outer region parallelisation
!!
!!The outer region parallelisation is somewhat easier to envisage than the
!!inner region, as it is actually a division of physical space. Two layers
!!of parallelism are employed in the outer region, one using MPI and one
!!using OpenMP.
!!
!!#### Layer 1
!!
!!The major division in the outer region is a division of the physical
!!space. Thus, an outer region of 100 a.u. might be divided into smaller
!!sectors of 25 a.u with each sector handled by an MPI task. Because the
!!outer region uses an explicit grid based representation of the
!!wavefunction, this corresponds to each MPI task handling a set number of
!!grid points.
!!
!! @image html outer_grid.png
!! @image latex outer_grid.png
!!
!!In practice, the outer region master (first MPI-task in the outer
!!region, highlighted red above) is allocated a smaller number of grid
!!points than the rest of the outer region, to allow additional resource
!!for the extra communication responsibilities with the inner region.
!!
!!Performance is enhanced by reducing the number of grid points per sector
!!(and increasing the number of outer-region MPI tasks to maintain the
!!size of the outer region). However, the finite-difference rule
!!implementation requires a minimum of 24 grid points per sector. If this
!!limit is reached, further performance can be extracted from the second
!!layer of parallelism.
!!
!!#### Layer 2
!!
!!As in the inner region, additional performance can be extracted with the
!!use of OpenMP parallelism of each MPI task. Thus a number of shared
!!memory threads can be allocated per outer region task (this is
!!controlled separately from the number of OpenMP threads in the inner
!!region). In the outer region, most of the calculation takes place for
!!each electron-emission channel independently of the other emission
!!channels, so all do loop structures which loop over the variable
!!`channel_ID` are parallelised with the `!$OMP PARALLEL DO` structure.
!!
!!As with the inner region, we have found that in practice the greatest
!!performance gains are obtained with providing additional MPI tasks
!!rather than OpenMP threads. However, because of the hard limit on how
!!small the outer region sectors can be made with additional MPI
!!parallelisation, the OpenMP layer becomes more useful in the outer
!!region, especially in systems with many electron-emission channels.

