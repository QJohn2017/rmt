! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief High level controller for the wavefunction files. Including reading and
!> writing wavefunction data, extracting observables (populations, expectation
!> values), and applying absorbing boundary. 

MODULE wavefunction

    USE precisn,                   ONLY: wp
    USE rmt_assert,                ONLY: assert
    USE calculation_parameters,    ONLY: single_ioniz_bndry_in_grid_pts
    USE communications_parameters, ONLY: outer_group_id
    USE grid_parameters,           ONLY: channel_id_1st, &
                                         channel_id_last, &
                                         x_1st, &
                                         x_last
    USE initial_conditions,        ONLY: propagation_order
    USE mpi_communications,        ONLY: i_am_inner_master, &
                                         i_am_outer_master, &
                                         i_am_in_inner_region, &
                                         i_am_in_outer_region, &
                                         my_block_id
    USE MPI



    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE, SAVE :: psi_outer(:, :, :)
    COMPLEX(wp), ALLOCATABLE, SAVE :: psi_inner(:, :)

    COMPLEX(wp), ALLOCATABLE, SAVE :: outer_initial_state_at_b(:, :)

    INTEGER, PARAMETER :: data_1st = 0
    INTEGER            :: data_last   != steps_per_run - 1
    INTEGER            :: data_array_length != data_last - data_1st + 1

    REAL(wp), ALLOCATABLE    :: pop_all_outer(:)
    REAL(wp), ALLOCATABLE    :: pop_all_inner(:)
    REAL(wp), ALLOCATABLE    :: pop_all(:)
    REAL(wp), ALLOCATABLE    :: pop_all_outer_si(:)
    REAL(wp), ALLOCATABLE    :: pop_all_outer_ryd(:)
    REAL(wp)    :: pop_all_outer_L
    REAL(wp)    :: norm_all_outer, norm_all_inner, norm_all
    REAL(wp)    :: population_at_this_pt

    REAL(wp), ALLOCATABLE    :: population_all(:, :)           ! = 0.0_wp
    REAL(wp), ALLOCATABLE    :: population_all_inner(:, :)     ! = 0.0_wp
    REAL(wp), ALLOCATABLE    :: population_all_outer(:, :)     ! = 0.0_wp
    REAL(wp), ALLOCATABLE    :: population_all_outer_si(:, :)  ! = 0.0_wp
    REAL(wp), ALLOCATABLE    :: population_all_outer_ryd(:, :) ! = 0.0_wp
    REAL(wp), ALLOCATABLE    :: population_gs_inner(:, :)      ! = 0.0_wp
    REAL(wp), ALLOCATABLE    :: population_at_R1st(:, :)       ! = 0.0_wp
    
    COMPLEX(wp), ALLOCATABLE :: expec_all(:,:,:)
    COMPLEX(wp), ALLOCATABLE :: expec_outer(:,:,:)
    COMPLEX(wp), ALLOCATABLE :: expec_inner(:,:,:)
    
    REAL(wp), ALLOCATABLE, SAVE :: discarded_pop_outer(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: delta_pop_outer(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: discarded_pop_outer_si(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: delta_pop_outer_si(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: population_all_outer_L(:, :, :)

    REAL(wp), ALLOCATABLE :: max_eval(:) !  = 0.0_wp
    REAL(wp), ALLOCATABLE :: min_eval(:) !  = 0.0_wp

    REAL(wp), ALLOCATABLE :: hr(:, :, :)

    REAL(wp), ALLOCATABLE :: eigstates(:, :, :)
    REAL(wp), ALLOCATABLE :: eigvals(:, :)

    REAL(wp), ALLOCATABLE :: time_in_au(:)  ! = 0.0_wp
    INTEGER               :: info

CONTAINS

    SUBROUTINE setup_wavefunction_module(stage, &
                                         stage_first, &
                                         nfdm, &
                                         krylov_hdt_desired, &
                                         delta_t, &
                                         propagation_order, &
                                         half_fd_order)

        USE calculation_parameters, ONLY: this_version_only_makes_ground
        USE initial_conditions,     ONLY: debug, write_ground

        INTEGER, INTENT(IN)  :: nfdm
        INTEGER, INTENT(IN)  :: stage
        INTEGER, INTENT(IN)  :: stage_first
        LOGICAL, INTENT(IN)  :: krylov_hdt_desired
        REAL(wp), INTENT(IN) :: delta_t
        INTEGER, INTENT(IN)  :: propagation_order
        INTEGER, INTENT(IN)  :: half_fd_order
        INTEGER              :: channel_id

        CALL setup_wavefunction_parameters

        CALL setup_data_arrays

        CALL initialize_wavefunction(stage, stage_first)

        CALL initialise_inner_and_outer_propagators(krylov_hdt_desired, &
                                                    delta_t, &
                                                    propagation_order, &
                                                    half_fd_order, &
                                                    nfdm)

        IF (i_am_inner_master .AND. debug) THEN
            PRINT *, 'Initialized Psi_Inner(1)=', psi_inner(1, :)
        END IF

        IF (i_am_outer_master .AND. debug) THEN
            DO channel_id = channel_id_1st, channel_id_last
                PRINT *, 'Initialized Channel_ID=', channel_id, &
                    ' Psi_Outer(X_1st, Channel_ID)=', psi_outer(x_1st, channel_id, 1)
            END DO
        END IF

        ! Optionally write ground state to file
        IF (stage == stage_first .AND. write_ground) THEN
            CALL write_ground_state
        END IF

        IF (this_version_only_makes_ground) THEN    ! don't update status
            CALL assert(.false.)
        END IF

    END SUBROUTINE setup_wavefunction_module

!---------------------------------------------------------------------------

    SUBROUTINE setup_wavefunction_parameters

        USE calculation_parameters, ONLY: steps_per_checkpt
        USE initial_conditions,     ONLY: numsols => no_of_field_confs

        INTEGER :: ierror

        data_last = steps_per_checkpt - 1
        data_array_length = data_last - data_1st + 1

        ALLOCATE (population_all(data_1st:data_last, 1:numsols), &
                  population_all_inner(data_1st:data_last, 1:numsols), &
                  population_all_outer(data_1st:data_last, 1:numsols), &
                  population_all_outer_si(data_1st:data_last, 1:numsols), &
                  population_all_outer_ryd(data_1st:data_last, 1:numsols), &
                  population_gs_inner(data_1st:data_last, 1:numsols), &
                  population_at_R1st(data_1st:data_last, 1:numsols), &
                  pop_all_outer(1:numsols), pop_all_inner(1:numsols), pop_all(1:numsols), pop_all_outer_si(1:numsols), &
                  pop_all_outer_ryd(1:numsols), expec_all(numsols, 3, 2), expec_inner(numsols, 3, 2), expec_outer(numsols, 3, 2), &
                  stat=ierror)

        CALL assert(ierror .EQ. 0, "Allocation error in wavefunction parameters")

        population_all(data_1st:data_last, 1:numsols) = 0.0_wp
        population_all_inner(data_1st:data_last, 1:numsols) = 0.0_wp
        population_all_outer(data_1st:data_last, 1:numsols) = 0.0_wp
        population_all_outer_si(data_1st:data_last, 1:numsols) = 0.0_wp
        population_all_outer_ryd(data_1st:data_last, 1:numsols) = 0.0_wp
        population_gs_inner(data_1st:data_last, 1:numsols) = 0.0_wp
        population_at_R1st(data_1st:data_last, 1:numsols) = 0.0_wp

        ALLOCATE (max_eval(data_1st:data_last), &
                  min_eval(data_1st:data_last), &
                  hr(0:propagation_order, 0:propagation_order, 1:numsols), &
                  eigstates(0:propagation_order, 0:propagation_order, 1:numsols), &
                  eigvals(0:propagation_order, 1:numsols), &
                  time_in_au(data_1st:data_last), &
                  stat=ierror)

        CALL assert(ierror .EQ. 0, "Allocation error in wavefunction parameters")

        max_eval(data_1st:data_last) = 0.0_wp
        min_eval(data_1st:data_last) = 0.0_wp
        time_in_au(data_1st:data_last) = 0.0_wp

    END SUBROUTINE setup_wavefunction_parameters

!---------------------------------------------------------------------------

    SUBROUTINE setup_data_arrays

        USE initial_conditions, ONLY: numsols => no_of_field_confs

        INTEGER :: err

        IF (i_am_in_inner_region) THEN
            CALL setup_inner_region_wavefunction
        END IF

        IF (i_am_in_outer_region) THEN
            CALL setup_outer_region_wavefunction
        END IF

        ! Allocations required in both inner and outer

        ALLOCATE (outer_initial_state_at_b(channel_id_1st:channel_id_last, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Outer_Initial_State_at_b')

        outer_initial_state_at_b = (0.0_wp, 0.0_wp)

        norm_all_inner = 1.0_wp
        norm_all_outer = 0.0_wp
        norm_all = 1.0_wp

        pop_all_outer = 0.0_wp
        pop_all_inner = 0.0_wp
        pop_all = 0.0_wp
        pop_all_outer_si = 0.0_wp
        pop_all_outer_L = 0.0_wp

        population_at_this_pt = 0.0_wp

        population_all = 0.0_wp
        max_eval = 0.0_wp
        min_eval = 0.0_wp

        expec_all = (0.0_wp, 0.0_wp)
        expec_inner = (0.0_wp, 0.0_wp)
        expec_outer = (0.0_wp, 0.0_wp)

        time_in_au = 0.0_wp

    END SUBROUTINE setup_data_arrays

!---------------------------------------------------------------------------

    SUBROUTINE setup_inner_region_wavefunction

        USE initial_conditions, ONLY: numsols => no_of_field_confs
        USE readhd, ONLY: max_L_block_size

        INTEGER :: err

        ALLOCATE (psi_inner(1:max_L_block_size, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Psi_Inner')

        psi_inner = (0.0_wp, 0.0_wp)

        ! initialize data arrays too
        population_all_inner = 0.0_wp
        population_gs_inner = 0.0_wp

    END SUBROUTINE setup_inner_region_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE setup_outer_region_wavefunction

        USE initial_conditions, ONLY: numsols => no_of_field_confs, &
                                      keep_popL

        INTEGER :: err

        ! Outer region allocations and initializations

        ALLOCATE (psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Psi_Outer')

        ALLOCATE (discarded_pop_outer(channel_id_1st:channel_id_last, 1:numsols), &
                  stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Discarded_Pop_outer')

        ALLOCATE (delta_pop_outer(channel_id_1st:channel_id_last, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Delta_Pop_outer')

        ALLOCATE (discarded_pop_outer_si(channel_id_1st:channel_id_last, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Discarded_Pop_outer_SI')

        ALLOCATE (delta_pop_outer_si(channel_id_1st:channel_id_last, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Delta_Pop_outer_SI')

        IF (keep_popL) THEN
            ALLOCATE (population_all_outer_L(data_1st:data_last, channel_id_1st:channel_id_last, 1:numsols), stat=err)
            CALL assert(err .EQ. 0, 'allocation error with Population_All_Outer_L')
            population_all_outer_L = 0.0_wp
        END IF

        psi_outer = (0.0_wp, 0.0_wp)
        discarded_pop_outer = 0.0_wp
        delta_pop_outer = 0.0_wp
        discarded_pop_outer_si = 0.0_wp
        delta_pop_outer_si = 0.0_wp

        ! initialize data arrays too
        population_all_outer = 0.0_wp
        population_all_outer_si = 0.0_wp
        population_at_R1st = 0.0_wp

    END SUBROUTINE setup_outer_region_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE initialize_wavefunction(stage, stage_first)

        USE initial_conditions,        ONLY: debug,                  &
                                             numsols => no_of_field_confs, &
                                             read_initial_wavefunction_from_file
        USE communications_parameters, ONLY: inner_group_id
        USE io_routines,               ONLY: read_my_wavefunc_inner, &
                                             read_my_wavefunc_outer
        USE inner_to_outer_interface,  ONLY: get_outer_initial_state_at_b
        USE mpi_communications,        ONLY: get_my_pe_id, &
                                             first_pes_share_cmplx_from
        USE mpi_layer_lblocks,         ONLY: i_am_gs_master, &
                                             pe_gs_id

        INTEGER, INTENT(IN) :: stage
        INTEGER, INTENT(IN) :: stage_first
        INTEGER             :: my_pe_id
        INTEGER             :: pe_gs_id_loc
        INTEGER             :: ierror

        IF (stage < stage_first) THEN
            PRINT *, 'ERROR Stage < Stage_First, need to initialize status file to -1 -1'
        END IF
        ! Initialize wavefunction
        IF ((stage > stage_first).OR.read_initial_wavefunction_from_file) THEN
            IF (i_am_in_outer_region) THEN
                CALL read_my_wavefunc_outer(psi_outer, &
                                            my_block_id, &
                                            discarded_pop_outer, &
                                            discarded_pop_outer_si)
            END IF

            IF (i_am_in_inner_region) THEN
                CALL read_my_wavefunc_inner(psi_inner, &
                                            my_block_id, &
                                            .true.)
            END IF
        END IF

        ! First stage, psi_inner should have coefficient of basis set
        !   member corresponding to initial state = 1
        ! First stage, psi_outer at r=b should be same as psi_inner at r=b

        CALL get_my_pe_id(my_pe_id)
        pe_gs_id_loc = 0
        IF (i_am_in_inner_region) THEN
            IF (i_am_gs_master) THEN
                pe_gs_id_loc = my_pe_id
            END IF
        END IF
        CALL MPI_REDUCE(pe_gs_id_loc, pe_gs_id, 1, MPI_INTEGER, &
                        MPI_SUM, 0, MPI_COMM_WORLD, ierror)
        CALL MPI_BCAST(pe_gs_id, 1, MPI_INTEGER, &
                       0, MPI_COMM_WORLD, ierror)

        IF ((stage == stage_first).AND..NOT.read_initial_wavefunction_from_file) THEN
            IF (i_am_in_inner_region) THEN
                IF (i_am_gs_master) THEN
                    ! INITIAL CONDITION for inner region wavefunction
                    psi_inner(1, :) = (1.0_wp, 0.0_wp)
                    CALL get_outer_initial_state_at_b(outer_initial_state_at_b, psi_inner)
                END IF
            END IF

            ! inner region 1st processor to send this to outer region 1st processor
            CALL first_pes_share_cmplx_from(inner_group_id, &
                                            outer_initial_state_at_b, &
                                            (channel_id_last - channel_id_1st + 1)*numsols, &
                                            i_am_gs_master, &
                                            i_am_outer_master, pe_gs_id)
            IF (i_am_outer_master) THEN
                psi_outer(x_1st, :, :) = outer_initial_state_at_b(channel_id_1st:channel_id_last, :)
            END IF
        END IF

        IF (debug) PRINT *, 'HUGO initialisation done', my_pe_id

    END SUBROUTINE initialize_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_wavefunction

        INTEGER  :: err

        ! Deallocations:
!       DEALLOCATE(psi_inner_on_grid, stat=err)
!       CALL assert(err .EQ. 0, 'deallocation error with psi_inner_on_grid')

        DEALLOCATE (outer_initial_state_at_b, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with Outer_Initial_State_at_b')

        IF (i_am_in_inner_region) THEN
            DEALLOCATE (psi_inner, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with psi_inner')
        END IF

        IF (i_am_in_outer_region) THEN
            ! Deallocate the arrays used in the propagation routines:
            DEALLOCATE (discarded_pop_outer, delta_pop_outer, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with pop outer arrays')

            DEALLOCATE (discarded_pop_outer_si, delta_pop_outer_si, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with pop outer arrays SI')

            IF (ALLOCATED(population_all_outer_L)) THEN
                DEALLOCATE (population_all_outer_L, stat=err) !Pop_All_Outer_L, stat=err)
                CALL assert(err .EQ. 0, 'deallocation error with pop outer L')
            END IF

        END IF

    END SUBROUTINE deallocate_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE setup_output_for_individual_dipole_file(expec_outer,expec_inner,expec_all)

        USE global_linear_algebra, ONLY: get_global_expec_Z_outer
        USE initial_conditions,    ONLY: numsols => no_of_field_confs
        USE live_communications,   ONLY: get_global_expec_Z_inner
        USE mpi_communications,    ONLY: first_pes_share_cmplx_val
        
        COMPLEX(wp),INTENT(IN) :: expec_outer(1:numsols)
        COMPLEX(wp),INTENT(IN) :: expec_inner(1:numsols)
        COMPLEX(wp),INTENT(OUT) :: expec_all(1:numsols)

        COMPLEX(wp) :: expec_all_outer(1:numsols)
        COMPLEX(wp) :: expec_all_inner(1:numsols)

        INTEGER :: isol

        expec_all_inner(:) = (0.0_wp,0.0_wp)
        expec_all_outer(:) = (0.0_wp,0.0_wp)

        DO isol = 1, numsols
            If (i_am_in_outer_region) THEN
                CALL get_global_expec_Z_outer(expec_outer(isol), expec_all_outer(isol))
            END IF

            IF (i_am_in_inner_region) THEN
                CALL get_global_expec_Z_inner(expec_inner(isol), expec_all_inner(isol))
            END IF

            CALL first_pes_share_cmplx_val(outer_group_id, expec_all_outer(isol), i_am_inner_master, i_am_outer_master)
        END DO

        expec_all = expec_all_inner + expec_all_outer

    END SUBROUTINE

!---------------------------------------------------------------------------

    SUBROUTINE output_dipole_files(iteration_start_time, iteration_finish_time, current_field_strength)

        USE initial_conditions,    ONLY: dipole_velocity_output,        &
                                         dipole_dimensions_desired,     &
                                         length_guage_id,               &
                                         velocity_guage_id,             &
                                         Hermitian_Tolerance
        USE io_files,              ONLY: write_output_files

        REAL(wp), INTENT(IN) :: iteration_start_time, iteration_finish_time
        REAL(wp), DIMENSION(3), INTENT(IN) :: current_field_strength
        Character(Len=1) :: Dimension_Char(1:3) = (/'x','y','z'/)

        INTEGER :: Dimension_Counter

        Do Dimension_Counter=1,3
            IF (dipole_dimensions_desired(Dimension_Counter)) THEN

                Call setup_output_for_individual_dipole_file(expec_outer(:,Dimension_Counter,length_guage_id),      &
                                                             expec_inner(:,Dimension_Counter,length_guage_id),      &
                                                             expec_all(:,Dimension_Counter,length_guage_id))

                ! the expectation value of an hermitian operator should be real:
                CALL assert(ALL(ABS(AIMAG(expec_all(:,Dimension_Counter,1))) < Hermitian_Tolerance), &
                "expectation value length is not real: is dipole operator hermitian in "        &
                // Dimension_Char(Dimension_Counter) // " Dimension? If there is no problem try"&
                // " a larger Hermitian_Tolerance option.")
            
                IF (dipole_velocity_output) THEN
                    CALL setup_output_for_individual_dipole_file(expec_outer(:,Dimension_Counter,velocity_guage_id),&
                                                             expec_inner(:,Dimension_Counter,velocity_guage_id),    &
                                                             expec_all(:,Dimension_Counter,velocity_guage_id))

                    ! the expectation value of an hermitian operator should be real:
                    CALL assert(ALL(ABS(AIMAG(expec_all(:,Dimension_Counter,velocity_guage_id))) < Hermitian_Tolerance), &
                    "expectation value velocity is not real: is dipole operator hermitian in "                      &
                    // Dimension_Char(Dimension_Counter) // " Dimension? If there is no problem try a larger"       &
                    // " Hermitian_Tolerance option.")
                END IF
            END IF
        END DO

        IF (i_am_inner_master) THEN
            CALL write_output_files(iteration_start_time, iteration_finish_time, current_field_strength, expec_all)
        END IF

    END SUBROUTINE output_dipole_files

!---------------------------------------------------------------------------

    SUBROUTINE absorb_wavefunction

        USE global_linear_algebra, ONLY: split
        USE initial_conditions,    ONLY: deltaR
        USE propagators,           ONLY: update_psi_outer, &
                                         update_outer_wavefunction

        ! Refresh Psi_Outer
        CALL update_psi_outer(psi_outer)

        ! Do the split
        CALL split(deltaR, &
                   psi_outer, &
                   delta_pop_outer, &
                   delta_pop_outer_si, &
                   single_ioniz_bndry_in_grid_pts)

        ! Update discarded population sums
        discarded_pop_outer = discarded_pop_outer + delta_pop_outer
        discarded_pop_outer_si = discarded_pop_outer_si + delta_pop_outer_si

        ! Save changes to psi_outer in the outer region propagation routine
        CALL update_outer_wavefunction(psi_outer)

    END SUBROUTINE absorb_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE outer_wavefunction_update(pop_index)

        USE global_linear_algebra, ONLY: get_global_pop_outer, &
                                         get_global_pop_outer_L
        USE initial_conditions,    ONLY: deltaR,&
                                         keep_popL
        USE propagators,           ONLY: update_psi_outer

        INTEGER, INTENT(IN) :: pop_index

        ! Refresh psi_outer
        CALL update_psi_outer(psi_outer)

        ! Calculate population - only 1st PE in outer region returns correct value
        CALL get_global_pop_outer(deltaR, &
                                  psi_outer, &
                                  pop_all_outer, &
                                  discarded_pop_outer, &
                                  pop_all_outer_si, &
                                  discarded_pop_outer_si, &
                                  pop_all_outer_ryd, &
                                  single_ioniz_bndry_in_grid_pts)

        IF (keep_popL) THEN                                  
            CALL get_global_pop_outer_L(deltaR, &
                                        psi_outer, &
                                        population_all_outer_L, &
                                        data_1st, data_last, pop_index, &
                                        discarded_pop_outer)
        END IF

        ! Update population arrays
        population_all_outer(pop_index, :) = pop_all_outer
        population_all_outer_si(pop_index, :) = pop_all_outer_si
        population_all_outer_ryd(pop_index, :) = pop_all_outer_ryd

    END SUBROUTINE outer_wavefunction_update

!---------------------------------------------------------------------------

    SUBROUTINE inner_wavefunction_update(pop_index)

        USE krylov_method,       ONLY: update_psi_inner
        USE live_communications, ONLY: get_global_pop_inner

        INTEGER, INTENT(IN) :: pop_index

        ! Refresh psi_inner
        CALL update_psi_inner(psi_inner)

        ! Calculate population
        CALL get_global_pop_inner(psi_inner, pop_all_inner)

        ! Update population arrays
        population_all_inner(pop_index, :) = pop_all_inner
        population_gs_inner(pop_index, :) = ABS(psi_inner(1, :))**2

    END SUBROUTINE inner_wavefunction_update

!---------------------------------------------------------------------------

    SUBROUTINE wavefunction_update(pop_index, &
                                   iteration_finish_time, &
                                   current_E_field_strength, &
                                   timeindex, &
                                   my_post)

        USE calculation_parameters, ONLY: wavefunction_output_desired
        USE initial_conditions,     ONLY: timesteps_per_output, &
                                          numsols => no_of_field_confs
        USE io_files,               ONLY: write_pop_files
        USE io_routines,            ONLY: write_inner_wavefunction_files
        USE mpi_communications,     ONLY: first_pes_share_val_from, &
                                          first_pes_share_real_from, &
                                          gs_master_shares_val
        USE mpi_layer_Lblocks,      ONLY: i_am_block_master, pe_gs_id, i_am_gs_master

        REAL(wp), INTENT(IN)   :: iteration_finish_time
        REAL(wp), INTENT(IN)   :: current_E_field_strength(3, numsols)
        INTEGER, INTENT(IN)    :: timeindex
        INTEGER, INTENT(IN)    :: pop_index
        INTEGER, INTENT(IN)    :: my_post
        REAL(wp)               :: pop_GS(numsols)

        IF (i_am_in_outer_region) THEN
            CALL outer_wavefunction_update(pop_index)
        END IF

        IF (i_am_in_inner_region) THEN
            CALL inner_wavefunction_update(pop_index)
        END IF

        ! Get totals on the 1st PE
        ! 1st PE outer sends Pop_All_Outer to 1st PE in inner
        CALL first_pes_share_real_from(outer_group_id, &
                                      pop_all_outer, &
                                      numsols, &
                                      i_am_inner_master, &
                                      i_am_outer_master)
        ! 1st PE outer sends Pop_All_Outer_SI to 1st PE in inner
        CALL first_pes_share_real_from(outer_group_id, &
                                      pop_all_outer_si, &
                                      numsols, &
                                      i_am_inner_master, &
                                      i_am_outer_master)
        ! 1st PE outer sends Pop_All_Outer_SI to 1st PE in inner
        CALL first_pes_share_real_from(outer_group_id, &
                                      pop_all_outer_ryd, &
                                      numsols, &
                                      i_am_inner_master, &
                                      i_am_outer_master)

        ! GS master sends ground state population to inner master                                      
        pop_GS(:) = population_gs_inner(pop_index, :)
        CALL gs_master_shares_val(pop_GS,&
                                  i_am_gs_master, &
                                  i_am_inner_master,&
                                  pe_gs_id)
        ! only 1st PE has correct value for Pop_All
        pop_all = pop_all_outer + pop_all_inner
        population_all(pop_index, :) = pop_all

        CALL write_pop_files(iteration_finish_time, &
                             pop_all, &
                             pop_all_inner, &
                             pop_GS, &
                             pop_all_outer, &
                             pop_all_outer_si, &
                             pop_all_outer_ryd, &
                             current_E_field_strength, &
                             propagation_order, &
                             timeindex, &
                             i_am_outer_master, &
                             i_am_inner_master)

        IF ((i_am_in_inner_region) .AND. (wavefunction_output_desired)) THEN

            ! only master cores of each block and those handling order 0 need to write:
            IF (i_am_block_master) THEN
                CALL write_inner_wavefunction_files(psi_inner, &
                                                    timeindex, &
                                                    timesteps_per_output, &
                                                    my_post, &
                                                    my_block_id)
            END IF

        END IF

    END SUBROUTINE wavefunction_update

!---------------------------------------------------------------------------

    SUBROUTINE get_and_print_eigs(pop_index)

        USE initial_conditions,      ONLY: debug, &
                                           numsols => no_of_field_confs
        USE eigenstates_in_kryspace, ONLY: get_eigstuff_for_tridiag_h
        USE mpi_communications,      ONLY: first_pes_share_real2_from

        INTEGER, INTENT(IN) :: pop_index
        INTEGER             :: ii, info, isol

        ! get max eigenvalue of h - Outer Region only
        IF (i_am_in_outer_region) THEN
            DO isol = 1, numsols
                CALL get_eigstuff_for_tridiag_h(hr(:,:,isol), eigstates(:,:,isol), eigvals(:,isol), info)
            END DO
            min_eval(pop_index) = eigvals(0, 1)
            max_eval(pop_index) = eigvals(propagation_order, 1)
        END IF

        ! 1st PE outer sends Eigvals to 1st PE in inner
        CALL first_pes_share_real2_from(outer_group_id, &
                                       eigvals, &
                                       propagation_order + 1, &
                                       numsols, &
                                       i_am_inner_master, &
                                       i_am_outer_master)

        ! 1st PE outer sends Eigstates to 1st PE in inner
        DO isol = 1, numsols
            CALL first_pes_share_real2_from(outer_group_id, &
                                            eigstates(:, :, isol), &
                                            propagation_order + 1, &
                                            propagation_order + 1, &
                                            i_am_inner_master, &
                                            i_am_outer_master)
        END DO

        IF (i_am_inner_master) THEN
            PRINT *, 'Outer Region Norm = ', pop_all_outer
            IF (debug) THEN
                PRINT *, 'Eigenenergies for outer Hamiltonian in krylov subspace for the first solution'
                PRINT *, eigvals(:, 1)
                PRINT *, 'Population in krylov eigenstates for the first solution'
                PRINT *, ((eigstates(0, ii, 1)*SQRT(pop_all_outer(1)))**2, ii=0, propagation_order)
            END IF
        END IF

    END SUBROUTINE get_and_print_eigs

!---------------------------------------------------------------------------

    SUBROUTINE write_ground_state

        USE io_routines,       ONLY: write_ground_wavefunc_outer, &
                                     write_ground_wavefunc_inner
        USE mpi_layer_Lblocks, ONLY: i_am_block_master

        IF (i_am_in_outer_region) THEN
            CALL write_ground_wavefunc_outer(psi_outer, my_block_id)
        END IF

        IF (i_am_in_inner_region) THEN
            CALL write_ground_wavefunc_inner(psi_inner, &
                                             my_block_id, &
                                             i_am_block_master)
        END IF

    END SUBROUTINE write_ground_state

!---------------------------------------------------------------------------

    SUBROUTINE initialise_inner_and_outer_propagators(krylov_hdt_desired, &
                                                      delta_t, &
                                                      propagation_order, &
                                                      half_fd_order, &
                                                      nfdm)
        USE krylov_method, ONLY: init_krylov_module
        USE propagators,   ONLY: initialise_propagators

        LOGICAL, INTENT(IN)  :: krylov_hdt_desired
        REAL(wp), INTENT(IN) :: delta_t
        INTEGER, INTENT(IN)  :: propagation_order
        INTEGER, INTENT(IN)  :: half_fd_order
        INTEGER, INTENT(IN)  :: nfdm

        ! Initialize inner region propagation parameters and arrays
        ! Initialize wfa in inner region propagation routines

        IF (i_am_in_inner_region) THEN
            CALL init_krylov_module(propagation_order, psi_inner)
        END IF

        ! Initialize outer region propagation parameters and arrays
        ! Initialize Outer_Wavefunction in outer region propagation routines

        IF (i_am_in_outer_region) THEN
            CALL initialise_propagators(krylov_hdt_desired, delta_t, psi_outer, &
                                        propagation_order, nfdm, half_fd_order, &
                                        i_am_outer_master)
        END IF

    END SUBROUTINE initialise_inner_and_outer_propagators

!---------------------------------------------------------------------------

    SUBROUTINE update_wavefunction_module(timeindex, &
                                          iteration_start_time, &
                                          iteration_finish_time, &
                                          current_field_strength, &
                                          pop_index, &
                                          my_post)

        USE initial_conditions, ONLY: dipole_output_desired, &
                                      timesteps_per_output, &
                                      absorb_desired, &
                                      numsols => no_of_field_confs
        USE work_at_intervals,  ONLY: times_come_to_do_the_following

        INTEGER, INTENT(IN)    :: timeindex
        REAL(wp), INTENT(IN)   :: iteration_start_time
        REAL(wp), INTENT(IN)   :: iteration_finish_time
        REAL(wp), INTENT(IN)   :: current_field_strength(3, numsols)
        INTEGER, INTENT(IN)    :: my_post

        INTEGER, INTENT(INOUT) :: pop_index
!       REAL(wp),INTENT(INOUT) :: time_in_au(:)

        !-----------------------------------------------------------------------
        ! EField and dipole expectation files are output on every timestep to
        ! improve resolution in data analysis
        !-----------------------------------------------------------------------

        IF (dipole_output_desired) THEN
            CALL output_dipole_files(iteration_start_time, iteration_finish_time, current_field_strength)
        END IF

        IF (times_come_to_do_the_following(timeindex, timesteps_per_output)) THEN

            IF ((absorb_desired) .AND. (i_am_in_outer_region)) THEN
                CALL absorb_wavefunction
            END IF

            pop_index = pop_index + 1
            CALL wavefunction_update(pop_index, &
                                     iteration_finish_time, &
                                     current_field_strength, &
                                     timeindex, &
                                     my_post)

            time_in_au(pop_index) = iteration_finish_time

            CALL get_and_print_eigs(pop_index)

        END IF ! Times come to do the following

    END SUBROUTINE update_wavefunction_module

END MODULE wavefunction

!> @page rmtout2 RMT: Output
!! @brief Describes RMT output files and directories
!!
!!@tableofcontents
!!
!!@section out_files List of output files
!!
!!The default and optional RMT output files, which can be found in the job submission directory,
!! are described on the following page: @subpage comp_output1.
!!
!!@section out_dirs Output directories
!!
!! The contents of the directories 'ground', 'state' and 'data' are described in more detail on the following page:
!! @subpage comp_output2.


!> @page comp_output1 Output Files
!! @brief Describes the output files
!! @tableofcontents
!! 
!! @section def_files Default Files
!!
!!The following files are updated during the calculation every
!!`Timesteps_Per_Output` iterations
!!
!!* `CurrentPosition`
!!    * Summary of the current status of the calculation (key parameters, and
!!      values of variables)
!!* `rmt.log`
!!    * Output printed by executing code 
!!    * Debug information during setup
!!    * Summary of the current status of the calculation 
!!* `pop_all.<version_number>`
!!* `pop_inn.<version_number>`
!!* `pop_out.<version_number>`
!!    * Contain the total/inner/outer population
!!* `timing_inner.<version_number>`
!!    * Timing information recorded on the inner region master
!!* `timing_outer0.<version_number>`
!!    * Timing information recorded on the outer region master
!!* `timing_outer1.<version_number>`
!!    * Timing information recorded on the first (non-master) outer core
!!    * The two outer timing files allow the balancing of work on the outer region
!!      master
!!* `EField.<version_number>`
!!    * The electric field strength (in a.u) in component form (x,y,z components) 
!!
!!Additionally:
!!
!!* `hstat.<version_number>`
!!    * Checkpoint status file which is read by the RMT code for restart
!!    * Is written only after the latest Checkpoint
!!
!!
!!@section opt_output Optional outputs
!!
!!* `expec_z_all.<version_number>` 
!!  `(dipole_output_desired=true)`
!!    * The expectation value of the dipole length operator at every time-step 
!!* `expec_v_all.<version_number>`
!!  `(dipole_velocity_output=true)` and `(dipole_output_desired=true)`
!!    * The expectation value of the dipole velocity operator at every time-step 
!!
!!Note, that if `dipole_output_desired=true` then the electric field is also
!!output at every time step- this can be necessary to obtain high-resolution,
!!high-energy Fourier Transforms of the output data.
!!
!!@section out_dirs_ref Output directories
!!
!! The contents of the directories 'ground', 'state' and 'data' are described in more detail on the following page:
!! @ref comp_output2

