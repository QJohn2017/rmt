! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!
!> @ingroup source
!> @brief Handles all aspects of calculating the electric field strength at a given
!> point in time.

MODULE electric_field

    USE precisn,            ONLY: wp
    USE global_data,        ONLY: pi
    USE rmt_assert,         ONLY: assert
    USE initial_conditions, ONLY: ellipticity, &
                                  ellipticity_XUV, &
                                  frequency, &
                                  frequency_XUV, &
                                  periods_of_ramp_on, &
                                  periods_of_ramp_on_XUV, &
                                  periods_of_pulse, &
                                  periods_of_pulse_XUV, &
                                  ceo_phase_rad, &
                                  ceo_phase_rad_XUV, &
                                  delay_XUV_in_IR_periods, &
                                  delay_between_IR_fields

    IMPLICIT NONE

    REAL(wp), ALLOCATABLE :: userdef_field(:, :, :)     !< Additional user-defined field to be included in the calculation.
    REAL(wp), ALLOCATABLE :: userdef_field_times(:)     !< Time values for the samples in userdef_field

    PRIVATE
    PUBLIC init_electric_field_module
    PUBLIC get_field_components
    PUBLIC get_E_pulse
    PUBLIC field_sph_component

CONTAINS

    !> \brief   Get field values from file (if required)
    !> \authors J Benda
    !> \date    2019
    !>
    !> When use of an additional user-defined field is required, look for the field input file
    !> and read in all field values for use later. The filename is set to "EField.inp".
    !> It is expected that it has the same format as the field output file ("EField.XXX"), i.e.,
    !> the first column contains the simulated time, and other columns contain all three components
    !> of the field.
    !>
    SUBROUTINE init_electric_field_module

        USE initial_conditions, ONLY: use_field_from_file, &
                                      disk_path, &
                                      numsols => no_of_field_confs

        INTEGER :: fd, ierr, nrows, ncols, nfields
        REAL(wp), ALLOCATABLE :: table(:, :)

        IF (use_field_from_file) THEN

            ! open the file and read data from it
            IF (ALLOCATED(disk_path)) THEN
                OPEN (NEWUNIT = fd, FILE = disk_path // 'EField.inp', STATUS = 'old', ACTION = 'read', IOSTAT = ierr)
            ELSE
                OPEN (NEWUNIT = fd, FILE = 'EField.inp', STATUS = 'old', ACTION = 'read', IOSTAT = ierr)
            END IF
            CALL assert(ierr == 0, 'File EField.inp with custom field is not available.')
            CALL assert(read_table_from_file(fd, table), 'Error when reading data from EField.inp.')
            CLOSE (fd)

            ! check that the data are compatible with the calculation setup
            ncols = SIZE(table, 1)
            nrows = SIZE(table, 2)
            nfields = (ncols - 1) / 3
            CALL assert(3*nfields + 1 == ncols, 'Invalid format of EField.inp; must have 3*k+1 columns!')
            IF (nfields > numsols) THEN
                WRITE (*, '(A,I0,A,I0,A)') ' Warning: The file EField.inp contains ', nfields, &
                                           ' field(s); only ', numsols, ' will be used.'
            END IF
            IF (nfields < numsols) THEN
                WRITE (*, '(A,I0,A)') ' Warning: The file EField.inp contains only ', nfields, &
                                      ' field(s); they will be assigned to solutions in a round-robin way.'
            END IF

            ! store the data in the module arrays
            ALLOCATE (userdef_field(3, nfields, nrows), userdef_field_times(nrows), STAT = ierr)
            CALL assert(ierr == 0, 'Failed to allocate memory for user defined field.')
            userdef_field_times = table(1, 1:nrows)
            userdef_field = RESHAPE(table(2:ncols, 1:nrows), (/ 3, nfields, nrows /))

        END IF

    END SUBROUTINE init_electric_field_module


    !> \brief   Read real table from a file
    !> \authors J Benda
    !> \date    2019
    !>
    !> Reads a rectangular table of real numbers from the given file unit opened for formatted reading.
    !> Returns TRUE on success, FALSE on failure. Ignores empty lines and any text after those.
    !>
    LOGICAL FUNCTION read_table_from_file (fd, table) RESULT (status)

        INTEGER,               INTENT(IN)    :: fd
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: table(:, :)

        CHARACTER(LEN=:), ALLOCATABLE :: buffer
        INTEGER,          PARAMETER   :: bufsize = 100000
        INTEGER                       :: ierr, nrows, ncols, i, pos

        nrows  = 0
        ncols  = 0
        status = .TRUE.

        ! allocate line buffer
        ALLOCATE (CHARACTER(bufsize) :: buffer, STAT = ierr)
        CALL assert(ierr == 0, 'Error when allocating buffer in read_table_from_file')

        ! first pass through the file: count rows and columns
        REWIND (fd)
        row_loop: DO
            READ (fd, '(A)', IOSTAT = ierr) buffer
            IF (ierr /= 0 .OR. LEN_TRIM(buffer) == 0) EXIT row_loop
            nrows = nrows + 1
            IF (nrows == 1) THEN
                pos = 1
                col_loop: DO
                    i = VERIFY(buffer(pos:), ' ')   ! first non-space
                    IF (i == 0) EXIT col_loop
                    ncols = ncols + 1
                    pos = pos + i - 1
                    i = SCAN(buffer(pos:), ' ')     ! first space
                    pos = pos + i - 1
                END DO col_loop
            END IF
        END DO row_loop
        REWIND (fd)

        ! secod pass through the file: actually read the data
        ALLOCATE (table(ncols, nrows), STAT = ierr)
        CALL assert(ierr == 0, 'Error when allocating table in read_table_from_file')
        DO i = 1, nrows
            READ (fd, *, IOSTAT = ierr) table(:, i)
            IF (ierr /= 0) THEN
                status = .FALSE.
                RETURN
            END IF
        END DO

    END FUNCTION read_table_from_file


    !> \brief   Find non-zero components of field values of the pulse
    !> \authors J Benda
    !> \date    2019
    !>
    !> At the beginning of the calculation, RMT needs to find out which components of the electric
    !> field acquire non-zero values. This is required in \ref readhd::read_molecular_data (and
    !> elsewhere) when deciding which dipole couplings to include in the computation.
    !>
    !> The method to determine couplings that participate in a calculation is as follows:
    !>  1. Find orientation of the polarization ellipse, i.e., its minor and major semi-axis.
    !>  2. Check if the ellipse axes have any projections onto coordinate axes. Ignore the minor axis
    !>     when using linearly polarized field (i.e., with zero ellipticity of IR and XUV, and with
    !>     XUV that is not cross-polarized).
    !>  3. If the user-defined field (from file) is used, scan it and update the non-zero components.
    !>
    !> \return One logical flag for each component of the field indicating whether it evaluates to
    !>         non-zero value at some simulation time.
    !>
    FUNCTION get_field_components ()

        USE global_data,        ONLY: axis_drop_tol
        USE coordinate_system,  ONLY: coordinate_transform
        USE initial_conditions, ONLY: ellipticity, &
                                      ellipticity_XUV, &
                                      cross_polarized, &
                                      use_2colour_field, &
                                      use_field_from_file, &
                                      numsols => no_of_field_confs

        REAL(wp) :: minor(3), major(3)
        LOGICAL  :: get_field_components(3), linear
        INTEGER  :: i, isol

        get_field_components(:) = .FALSE.

        DO isol = 1, numsols

            minor = (/ 0, -1, 0 /)
            major = (/ 0,  0, 1 /)

            CALL coordinate_transform(minor, isol)
            CALL coordinate_transform(major, isol)

            ! ignore minor semi-axis when using linear polarization
            linear = (ellipticity(isol) == 0)
            IF (use_2colour_field) THEN
                linear = linear .AND. (ellipticity_XUV(isol) == 0) .AND. .NOT. cross_polarized(isol)
            END IF
            IF (linear) minor = 0

            DO i = 1, 3
                ! check if the polarization ellipse has non-empty projection onto this axis
                IF (ABS(minor(i)) > axis_drop_tol) get_field_components(i) = .TRUE.
                IF (ABS(major(i)) > axis_drop_tol) get_field_components(i) = .TRUE.

                ! check if the user-defined field has some non-zero components in this axis
                IF (use_field_from_file .AND. isol <= SIZE(userdef_field, 2)) THEN
                    IF (ANY(ABS(userdef_field(i, isol, :)) > axis_drop_tol)) get_field_components(i) = .TRUE.
                END IF
            END DO

        END DO

    END FUNCTION get_field_components


!---------------------------------------------------------------------------
! SECTION ON PULSE SHAPES
!---------------------------------------------------------------------------

    SUBROUTINE get_E_field_pulse_shape1(isol, periods_of_ramp_on, periods_of_pulse, &
                                        time, frequency, pulse_x, pulse_y, pulse_z)

        ! This is a circularly polarised pulse

        INTEGER,  INTENT(IN)  :: isol
        REAL(wp), INTENT(IN)  :: periods_of_pulse, periods_of_ramp_on
        REAL(wp), INTENT(IN)  :: time, frequency
        REAL(wp), INTENT(OUT) :: pulse_x, pulse_y, pulse_z
        REAL(wp)              :: nosc, midtime, phase

        ! This is an E FIELD PULSE with sin^2 ramp-on

        nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on
        midtime = 0.0_wp + (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency

        phase = ceo_phase_rad(isol)
        pulse_z = pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

        phase = phase - pi*0.5
        pulse_y = -ellipticity(isol)*pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

        pulse_x = 0.0_wp

    END SUBROUTINE get_E_field_pulse_shape1

!---------------------------------------------------------------------------

    SUBROUTINE get_E_field_pulse_shape2_IR(isol, periods_of_ramp_on, periods_of_pulse, &
                                           time, frequency, pulse_x, pulse_y, pulse_z, &
                                           delay_XUV_in_IR_periods, ceo_phase_rad)

        INTEGER,  INTENT(IN)  :: isol
        REAL(wp), INTENT(IN)  :: periods_of_pulse, periods_of_ramp_on
        REAL(wp), INTENT(IN)  :: time, frequency, delay_XUV_in_IR_periods, ceo_phase_rad
        REAL(wp), INTENT(OUT) :: pulse_x, pulse_y, pulse_z
        REAL(wp)              :: nosc, midtime, phase

        ! This is a circularly polarised pulse
        ! For IR Pulse

        nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on

        IF (delay_XUV_in_IR_periods < 0) THEN
            midtime = (-1.0_wp*delay_XUV_in_IR_periods + 0.5_wp*(nosc + 2.0_wp*periods_of_ramp_on))*2.0_wp*pi/frequency
        ELSE
            midtime = (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency
        END IF

        ! This is an E FIELD PULSE with sin^2 ramp-on

        phase = ceo_phase_rad
        pulse_z = pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

        phase = phase - pi*0.5
        pulse_y = -ellipticity(isol)*pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

        pulse_x = 0.0_wp

    END SUBROUTINE get_E_field_pulse_shape2_IR

!---------------------------------------------------------------------------

    SUBROUTINE get_E_field_pulse_shape2_XUV(isol, periods_of_ramp_on, periods_of_pulse, &
                                            time, frequency, pulse_x, pulse_y, pulse_z, &
                                            delay_XUV_in_IR_periods, ceo_phase_rad, frequency_IR)

        USE initial_conditions, ONLY: ellipticity_XUV, &
                                      cross_polarized

        INTEGER,  INTENT(IN)  :: isol
        REAL(wp), INTENT(IN)  :: periods_of_pulse, periods_of_ramp_on
        REAL(wp), INTENT(IN)  :: time, frequency, delay_XUV_in_IR_periods, ceo_phase_rad, frequency_IR
        REAL(wp), INTENT(OUT) :: pulse_x, pulse_y, pulse_z
        REAL(wp)              :: nosc, midtime, phase

        ! For linearly polarised pulse
        ! For XUV Pulse

        nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on

        IF (delay_XUV_in_IR_periods < 0) THEN
            midtime = (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency
        ELSE
            midtime = (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency + delay_XUV_in_IR_periods*2.0_wp*pi/frequency_IR
        END IF

        ! This is an E FIELD PULSE with sin^2 ramp-on

        IF (cross_polarized(isol)) THEN ! cross polarized linear pulses
            phase = ceo_phase_rad
            pulse_y = pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

            pulse_x = 0.0_wp
            pulse_z = 0.0_wp

        ELSE ! arbitrarily polarized XUV pulse
            phase = ceo_phase_rad
            pulse_z = pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

            phase = phase - pi*0.5
            pulse_y = -ellipticity_XUV(isol)*pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)

            pulse_x = 0.0_wp

        END IF

    END SUBROUTINE get_E_field_pulse_shape2_XUV

!---------------------------------------------------------------------------

    SUBROUTINE get_E_field_pulse_shape3_IR(isol, periods_of_ramp_on, periods_of_pulse, &
                                           time, frequency, pulse, delay_XUV_in_IR_periods, ceo_phase_rad)

        INTEGER,  INTENT(IN)  :: isol
        REAL(wp), INTENT(IN)  :: periods_of_pulse, periods_of_ramp_on
        REAL(wp), INTENT(IN)  :: time, frequency, delay_XUV_in_IR_periods, ceo_phase_rad
        REAL(wp), INTENT(OUT) :: pulse
        REAL(wp) :: nosc, midtime

!!!!!!!!!!!!!!!!!!! UNDER DEVELOPMENT -- DO NOT USE !!!!!!!!!!!!!!!!!!

        ! For XUV Pulse

        IF (delay_XUV_in_IR_periods < 0) THEN
            nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on
            midtime = (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency
        ELSE
            nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on
            midtime = (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency + delay_XUV_in_IR_periods*2.0_wp*pi/frequency
        END IF

        ! This is an E FIELD PULSE with sin^2 ramp-on

        pulse = pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, ceo_phase_rad)

    END SUBROUTINE get_E_field_pulse_shape3_IR

!---------------------------------------------------------------------------

    SUBROUTINE get_E_field_pulse_shape3_XUV(isol, periods_of_ramp_on, periods_of_pulse, &
                                            time, frequency, pulse, delay_XUV_in_IR_periods, ceo_phase_rad, frequency_IR, &
                                            periods_of_ramp_on_IR, periods_of_pulse_IR, delay_between_IR_fields)

        INTEGER,  INTENT(IN)  :: isol
        REAL(wp), INTENT(IN)  :: periods_of_pulse, periods_of_ramp_on
        REAL(wp), INTENT(IN)  :: periods_of_pulse_IR, periods_of_ramp_on_IR
        REAL(wp), INTENT(IN)  :: delay_between_IR_fields
        REAL(wp), INTENT(IN)  :: time, frequency, delay_XUV_in_IR_periods, ceo_phase_rad, frequency_IR
        REAL(wp), INTENT(OUT) :: pulse
        REAL(wp) :: nosc, midtime

!!!!!!!!!!!!!!!!!!! UNDER DEVELOPMENT -- DO NOT USE !!!!!!!!!!!!!!!!!!

        ! For XUV Pulse

        nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on

        IF (delay_between_IR_fields < 0) THEN
            nosc = periods_of_pulse_IR - 2.0_wp*periods_of_ramp_on_IR
            midtime = (nosc + 2.0_wp*periods_of_ramp_on_IR)*pi/frequency_IR
        ELSE
            nosc = periods_of_pulse - 2.0_wp*periods_of_ramp_on
            midtime = (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency + delay_XUV_in_IR_periods*2.0_wp*pi/frequency_IR
        END IF

        ! This is an E FIELD PULSE with sin^2 ramp-on

        pulse = pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, ceo_phase_rad)

    END SUBROUTINE get_E_field_pulse_shape3_XUV

!---------------------------------------------------------------------------
! SECTION ON INTENSITY
!---------------------------------------------------------------------------

!   SUBROUTINE init_intensity_parameters(intensity_value,intensity_value_2IR,intensity_value_XUV)

!       REAL(wp), INTENT(IN)  :: intensity_value,intensity_value_XUV, intensity_value_2IR

!       intensity = intensity_value
!       intensity2IR = intensity_value_2IR
!       intensity_XUV = intensity_value_XUV

!   END SUBROUTINE init_intensity_parameters


!---------------------------------------------------------------------------
! SECTION ON DETERMINATION OF FIELD
!---------------------------------------------------------------------------

    REAL(wp) FUNCTION E_field_in_au(isol)

        USE initial_conditions, ONLY: intensity

        INTEGER, INTENT(IN) :: isol
        REAL(wp) :: sqrt_of_intensity

        ! An E-field of 0.05336_wp atomic units is 10**14 W/cm**2

        sqrt_of_intensity = SQRT(intensity(isol))

        E_field_in_au = 0.05336_wp * sqrt_of_intensity / SQRT(1.0_wp + ellipticity(isol)*ellipticity(isol))    ! For a circularly polarised pulse

    END FUNCTION E_field_in_au

!---------------------------------------------------------------------------

    REAL(wp) FUNCTION E_field_in_au_IR2(isol)

        USE initial_conditions, ONLY: intensity2IR

        INTEGER, INTENT(IN) :: isol
        REAL(wp) :: sqrt_of_intensity

        ! An E-field of 0.05336_wp atomic units is 10**14 W/cm**2

        sqrt_of_intensity = SQRT(intensity2IR(isol))
        E_field_in_au_IR2 = 0.05336_wp*sqrt_of_intensity

    END FUNCTION E_field_in_au_IR2

!---------------------------------------------------------------------------

    REAL(wp) FUNCTION E_field_in_au_XUV(isol)

        USE initial_conditions, ONLY: intensity_XUV, ellipticity_XUV

        INTEGER, INTENT(IN) :: isol
        REAL(wp) :: sqrt_of_intensity

        ! An E-field of 0.05336_wp atomic units is 10**14 W/cm**2

        sqrt_of_intensity = SQRT(intensity_XUV(isol))
        E_field_in_au_XUV = 0.05336_wp * Sqrt_of_Intensity / SQRT(1.0_wp + ellipticity_XUV(isol)*ellipticity_XUV(isol))

    END FUNCTION E_field_in_au_XUV

!---------------------------------------------------------------------------

    FUNCTION E_pulse1(time, isol)
!   REAL(wp) FUNCTION E_pulse1 (time)

        REAL(wp), INTENT(IN)   :: time
        INTEGER,  INTENT(IN)   :: isol
        REAL(wp)               :: pulse_x, pulse_y, pulse_z

        REAL(wp), DIMENSION(3) :: E_pulse1

        E_pulse1 = 0.0_wp

        CALL get_E_field_pulse_shape1(isol, periods_of_ramp_on(isol), periods_of_pulse(isol), &
                                      time, frequency(isol), pulse_x, pulse_y, pulse_z)

        E_pulse1(1) = pulse_x*E_field_in_au(isol)

        E_pulse1(2) = pulse_y*E_field_in_au(isol)

        E_pulse1(3) = pulse_z*E_field_in_au(isol)

    END FUNCTION E_pulse1

!---------------------------------------------------------------------

    !> \brief Generate the instantaneous value electric field in the case where two pulses
    !> are employed.
    !
    !> Generate the instantaneous value electric field in the case where two pulses
    !> are employed. The prototype is an IR with a time-delayed XUV pulse, and the
    !> variables are named accordingly, but in general any two pulses can be
    !> employed. If Cross_Polarized=True then the two pulses are linearly polarized
    !> at right angles to each other. Otherwise, the primary (IR) pulse has its
    !> polarization set by the ellipticity parameter, and the XUV by the
    !> ellipticity_XUV parameter.
    FUNCTION E_pulse2(time, isol)

        REAL(wp), INTENT(IN)   :: time
        INTEGER,  INTENT(IN)   :: isol
        REAL(wp)               :: pulse_x, pulse_y, pulse_z
        REAL(wp), DIMENSION(3) :: E_pulse2_IR, E_pulse2_XUV
        REAL(wp), DIMENSION(3) :: E_pulse2

        E_pulse2_IR = 0.0_wp
        E_pulse2_XUV = 0.0_wp
        E_pulse2 = 0.0_wp

        ! IR
        CALL get_E_field_pulse_shape2_IR(isol, periods_of_ramp_on(isol), periods_of_pulse(isol), &
                                         time, frequency(isol), pulse_x, pulse_y, pulse_z, &
                                         delay_XUV_in_IR_periods(isol), ceo_phase_rad(isol))

        E_pulse2_IR(1) = pulse_x*E_field_in_au(isol)
        E_pulse2_IR(2) = pulse_y*E_field_in_au(isol)
        E_pulse2_IR(3) = pulse_z*E_field_in_au(isol)

        ! XUV
        CALL get_E_field_pulse_shape2_XUV(isol, periods_of_ramp_on_XUV(isol), periods_of_pulse_XUV(isol), &
                                          time, frequency_XUV(isol), pulse_x, pulse_y, pulse_z, &
                                          delay_XUV_in_IR_periods(isol), ceo_phase_rad_XUV(isol), frequency(isol))

        E_pulse2_XUV(1) = pulse_x*E_field_in_au_XUV(isol)
        E_pulse2_XUV(2) = pulse_y*E_field_in_au_XUV(isol)
        E_pulse2_XUV(3) = pulse_z*E_field_in_au_XUV(isol)

        ! Total

        E_pulse2 = E_pulse2_IR + E_pulse2_XUV

    END FUNCTION E_pulse2

!---------------------------------------------------------------------------

    REAL(wp) FUNCTION E_pulse3(time, isol)

        REAL(wp), INTENT(IN) :: time
        INTEGER,  INTENT(IN) :: isol
        REAL(wp)             :: pulse, E_pulse3_IR, E_pulse3_XUV, E_pulse3_IR2
        REAL(wp)             :: pulse_x, pulse_y, pulse_z

!!!!!!!!!!!!!!!!!!! UNDER DEVELOPMENT -- DO NOT USE !!!!!!!!!!!!!!!!!!

        ! IR
        CALL get_E_field_pulse_shape2_IR(isol, periods_of_ramp_on(isol), periods_of_pulse(isol), &
                                         time, frequency(isol), pulse_x, pulse_y, pulse_z, &
                                         delay_between_IR_fields(isol), ceo_phase_rad(isol))

        E_pulse3_IR = pulse*E_field_in_au(isol)

        ! XUV
        CALL get_E_field_pulse_shape3_XUV(isol, periods_of_ramp_on_XUV(isol), periods_of_pulse_XUV(isol), &
                                          time, frequency_XUV(isol), pulse, delay_XUV_in_IR_periods(isol), &
                                          ceo_phase_rad_XUV(isol), frequency(isol), periods_of_ramp_on(isol), &
                                          periods_of_pulse(isol), delay_between_IR_fields(isol))

        E_pulse3_XUV = pulse*E_field_in_au_XUV(isol)

        CALL get_E_field_pulse_shape3_IR(isol, periods_of_ramp_on(isol), periods_of_pulse(isol), &
                                         time, frequency(isol), pulse, delay_between_IR_fields(isol), ceo_phase_rad_XUV(isol))
        ! Total

        E_pulse3_IR2 = pulse*E_field_in_au_IR2(isol)

        E_pulse3 = E_pulse3_IR + E_pulse3_XUV + E_pulse3_IR2

    END FUNCTION E_pulse3

!---------------------------------------------------------------------------

    !> \brief   Return field value at a given time
    !>
    !> Sums all contributions from IR, XUV and the user-defined pulse, which has been read
    !> read from the file "EField.inp" durinf the initialization. The field value from the
    !> file is calculated as interpolation of the data read. If the current time is outside
    !> of the time interval covered by the field input file, zero is assumed instead.
    !>
    FUNCTION get_E_pulse(time, i)

        USE initial_conditions, ONLY: use_2colour_field, &
                                      use_leakage_pulse, &
                                      use_field_from_file
        USE coordinate_system, ONLY: coordinate_transform

        REAL(wp), INTENT(IN) :: time
        INTEGER,  INTENT(IN) :: i
        REAL(wp), DIMENSION(3) :: get_E_pulse

        IF (use_2colour_field) THEN
            IF (use_leakage_pulse) THEN
                get_E_pulse = E_pulse3(time, i)
            ELSE
                get_E_pulse = E_pulse2(time, i)
            END IF
        ELSE
            get_E_pulse = E_pulse1(time, i)
        END IF

        ! Transform the polarization plane using Euler angles
        CALL coordinate_transform(get_E_pulse, i)

        ! Optional additional user-defined field
        IF (use_field_from_file) THEN
            get_E_pulse = get_E_pulse + interpolate_userdef_field(time, i)
        END IF

    END FUNCTION get_E_pulse

!---------------------------------------------------------------------------

    !> \brief   Retrieve value of the user-defined field
    !> \authors J Benda
    !> \date    2019
    !>
    !> Interpolate field value from the table read from the custom field input file "EField.inp".
    !> When the current time is outside of the interval given in the file, return zeros for all
    !> components.
    !>
    !> The interpolation proceeds like this:
    !>   1. The available times are bisected to get the basic interval that contains the requested time.
    !>   2. The field values are linearly interpolated.
    !>
    !> The field file needs to contain at least two samples to result in non-zero field. Also,
    !> the time values need to be distinct, otherwise the linear interpolation will produce
    !> division by zero. Finally, the time values are expected to be ordered (in ascending way).
    !>
    FUNCTION interpolate_userdef_field (t, s)

        REAL(wp), INTENT(IN) :: t
        INTEGER,  INTENT(IN) :: s

        REAL(wp) :: interpolate_userdef_field(3), ti, tj, tm
        INTEGER :: i, j, m, f

        interpolate_userdef_field = 0
        f = 1 + MOD(s - 1, SIZE(userdef_field, 2))  ! cycle over available fields in the file

        IF (.NOT. ALLOCATED(userdef_field_times)) RETURN
        IF (SIZE(userdef_field_times) < 2) RETURN

        i = 1
        j = SIZE(userdef_field_times)

        DO
            ti = userdef_field_times(i)
            tj = userdef_field_times(j)

            IF (t < ti .OR. tj < t) THEN
                RETURN
            END IF

            IF (i + 1 == j) THEN
                interpolate_userdef_field = (userdef_field(:, f, i) * (tj - t) + userdef_field(:, f, j) * (t - ti)) / (tj - ti)
                RETURN
            END IF

            m = (i + j) / 2
            tm = userdef_field_times(m)

            i = MERGE(i, m, t <= tm)
            j = MERGE(m, j, t <= tm)
        END DO

    END FUNCTION interpolate_userdef_field

!---------------------------------------------------------------------------

    LOGICAL FUNCTION during_pulse(time, midtime, nosc, periods_of_ramp_on, frequency)

        REAL(wp), INTENT(IN) :: time
        REAL(wp), INTENT(IN) :: midtime
        REAL(wp), INTENT(IN) :: nosc
        REAL(wp), INTENT(IN) :: periods_of_ramp_on
        REAL(wp), INTENT(IN) :: frequency

        IF (ABS(time - midtime) < (nosc + 2.0_wp*periods_of_ramp_on)*pi/frequency) THEN
            during_pulse = .true.
        ELSE
            during_pulse = .false.
        END IF

    END FUNCTION during_pulse

!---------------------------------------------------------------------------

    LOGICAL FUNCTION during_peak(time, midtime, nosc, frequency)

        REAL(wp), INTENT(IN) :: time
        REAL(wp), INTENT(IN) :: midtime
        REAL(wp), INTENT(IN) :: nosc
        REAL(wp), INTENT(IN) :: frequency

        IF (ABS(time - midtime) < nosc*pi/frequency) THEN
            during_peak = .true.
        ELSE
            during_peak = .false.
        END IF

    END FUNCTION during_peak

!---------------------------------------------------------------------------

    REAL(wp) Function pulse_profile(time, midtime, nosc, periods_of_ramp_on, frequency, phase)
    ! given field parameters calculate a sin^2 pulse profile and return the electric field value at this time

        REAL(wp), INTENT(IN) :: time
        REAL(wp), INTENT(IN) :: midtime
        REAL(wp), INTENT(IN) :: nosc
        REAL(wp), INTENT(IN) :: periods_of_ramp_on
        REAL(wp), INTENT(IN) :: frequency
        REAL(wp), INTENT(IN) :: phase
        REAL(wp) :: prefac

        IF (during_pulse(time, midtime, nosc, periods_of_ramp_on, frequency)) THEN
            IF (during_peak(time, midtime, nosc, frequency)) THEN
                pulse_profile = COS(frequency*(time - midtime) + phase)
            ELSE ! if during ramp
                prefac = ABS(time - midtime) - nosc*pi/frequency
                prefac = 0.50_wp*(1.0_wp + COS(frequency*prefac/(2.0_wp*periods_of_ramp_on)))
                pulse_profile = COS(frequency*(time - midtime) + phase)*prefac
            END IF
        ELSE ! if not during pulse
            pulse_profile = 0.0_wp
        END IF

    END FUNCTION pulse_profile

!---------------------------------------------------------------------------

    !> \brief Get selected spherical component of a field
    !>
    !> The spherical vectors in RMT are calculated as follows (and in this order):
    !>
    !> \f[  n_+ =  \mathrm{i} (n_x + \mathrm{i} n_y) / \sqrt{2}  \f]
    !> \f[  n_0 =  \mathrm{i}  n_z                               \f]
    !> \f[  n_- = -\mathrm{i} (n_x - \mathrm{i} n_y) / \sqrt{2}  \f]
    !>
    !> The extra imaginary unit factor comes from the Fano-Racah phase convention used throughout the atomic
    !> code (here for compatibility with spherical harmonics that use the convention).
    !>
    !> \param field_cart  Cartesian components (1, 2, 3) of the electric field.
    !> \param idx         Spherical component (-1, 0, +1).
    !>
    PURE COMPLEX(wp) FUNCTION field_sph_component(field_cart, idx) RESUlT (compt)

        USE global_data, ONLY: im, rtwo

        REAL(wp), INTENT(IN) :: field_cart(3)
        INTEGER,  INTENT(IN) :: idx

        SELECT CASE (idx)
        CASE ( 1); compt = im * (im * field_cart(2) + field_cart(1)) / SQRT(rtwo)
        CASE ( 0); compt = im * field_cart(3)
        CASE (-1); compt = im * (im * field_cart(2) - field_cart(1)) / SQRT(rtwo)
        CASE DEFAULT; compt = 0
        END SELECT

    END FUNCTION field_sph_component

END MODULE electric_field
