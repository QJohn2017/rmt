! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets initial conditions for system: atomic/molecular/laser parameters,
!> parallel set up, checkpointing etc. Most parameters set in this module are
!> read from input.conf.

MODULE initial_conditions

    USE precisn,     ONLY: wp
    USE global_data, ONLY: pi

    IMPLICIT NONE

    INTEGER, PARAMETER :: RMatrixI_format_id = 1
    INTEGER, PARAMETER :: RMatrixII_format_id = 2

    Integer, PARAMETER :: LS_coupling_id = 1
    Integer, PARAMETER :: jK_coupling_id = 2

    INTEGER, PARAMETER :: length_guage_id=1
    INTEGER, PARAMETER :: velocity_guage_id=2

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !           TARGET INFORMATION          !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !> Magnetic quantum number of the ground state.
    !> For molecular calculations this parameter is ignored.
    !> @ingroup target_info
    INTEGER  :: ml = 0
    !> Defines the sum, modulo 2, of the angular momentum and
    !> parity of the ground state. EG for the ground state of Ne
    !> (singlet S even) L=0 and pi=0 thus lplusp = 0.
    !! @ingroup target_info
    INTEGER  :: lplusp = 0
    !> Maximum absolute value of ML to be used in the calculation.
    !> @ingroup target_info
    INTEGER  :: ML_max = -1
    !> Set a restriction on ML_max?
    LOGICAL  :: set_ML_max = .false.
    !> Defines which of the included symmetries is the ground state.
    !! @ingroup target_info
    INTEGER  :: gs_finast = 1      
    !> Defines if the calculation is for an atomic / molecular target.
    !> By default, the code assumes atomic.
    !! @ingroup target_info
    LOGICAL  :: molecular_target = .false.
    !> Defines which version of the RMatrix inner region codes used to generate the input data.
    !> The default is RMatrixII (2) but for relativistic calculations RMatrixI (1) will be used.
    !! @ingroup target_info
    INTEGER  :: dipole_format_id = RMatrixII_format_id
    !> defines which coupling scheme to use. The default is LS coupling (1); jK coupling (2)
    !> can also be employed. For molecular calculations this parameter is ignored.
    !> @ingroup target_info
    INTEGER  :: coupling_id = LS_coupling_id
    !> Defines whether or not the ground state energy should be adjusted.
    !! @ingroup target_info
    LOGICAL  :: adjust_gs_energy = .false.
    !> Defines the desired ground state energy in a.u.
    !! @ingroup target_info
    REAL(wp) :: gs_energy_desired  = 0.0_wp


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !            LASER INFORMATION          !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    ! If use_2colour_field = .true.  then set parameters for IR pulse and for XUV pulse below, and delay parameters
    ! If use_2colour_field = .false. then just need to correctly set parameters for pulse labelled as IR pulse below
    ! If use_leakage_pulse = .true.  then make sure to set the Starting_Intensity_IR2 parameter
    !> Allows the use of two independently controllable pulses.
    !! @ingroup laser_params
    LOGICAL  :: use_2colour_field = .false.
    !> Include a low intensity replica of the IR pulse locked in time with the XUV pulse
    !> Not an independently controllable pulse- the only parameter you can set is the intensity
    LOGICAL  :: use_leakage_pulse = .false.
    !> Add arbitrary field interpolated from file "EField.inp" (in the same format as the output EField.XXX)
    LOGICAL  :: use_field_from_file = .false.

    ! IR Pulse (Or if using 1 colour pulse):

    !> Number of different pulse characteristics to be solved in one run.
    INTEGER :: no_of_field_confs = 1
    !> Input namelist limit on number of different pulse characteristics to be solved in one run.
    INTEGER, PARAMETER :: max_sols = 1000

    !> The ellipticity of the primary pulse.
    !> If Ellipticity is not set in the input.conf file, the primary pulse
    !> is linearly polarized in the z direction (i.e. ellipticity = 0).
    !> Ellipticity = +1 gives left-hand-circ. polarized in the zy plane with E x dE/dt pointing to +x
    !> Ellipticity = -1 gives right-hand-circ. polarized in the zy plane with E x dE/dt pointing to -x
    !! @ingroup laser_params
    REAL(wp)  :: ellipticity(max_sols) = 0.0_wp ! +1 for LH, -1 for RH

    !> Frequency of the (primary) IR laser pulse in a.u.
    !! @ingroup laser_params
    REAL(wp)  :: frequency(max_sols) = 1.0_wp ! 27.212.5 eV
    !> Sets both the number of cycles to be used in the ramp on and off part of the pulse.
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_ramp_on(max_sols) = 2.5_wp
    !> Total number of field cycles including the ramp on and off.
    !> The pulse profile has a sin^2 envelope.
    !> Non-integer numbers of cycles are supported.
    !> periods_of_pulse will be greater than or equal to 2 * periods_of_ramp_on
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_pulse(max_sols) = 5.0_wp
    !> The carrier envelope phase measured in degrees
    !! @ingroup laser_params
    REAL(wp)  :: ceo_phase_deg(max_sols) = 0.0_wp
    !> Peak intensity of the (primary) IR pulse in units of 10**14 Wcm^-2
    !! @ingroup laser_params
    REAL(wp)  :: intensity(max_sols) = 1.0_wp
    !> Intensity of the leakage (secondary) IR pulse in units of 10**14 Wcm^-2
    REAL(wp)  :: intensity2IR(max_sols) = 0.0_wp

    ! Orientation of the polarisation plane in degrees. The angles specify intrinsic x-z'-x'' rotation.
    ! Default orientation (when all angles are zero) is: Ex = 0, Ey ~ -e*sin(t), Ez ~ cos(t).
    ! Now, for example, if a linear polarisation with direction
    !     ( sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta) )
    ! is desired, set the angles to alpha = 0, beta = phi - 90, gamma = -theta, and Ellipticity to 0.
    !> @ingroup laser_params
    REAL(wp)  :: euler_alpha(max_sols) = 0.0_wp
    !> @ingroup laser_params
    REAL(wp)  :: euler_beta(max_sols) = 0.0_wp
    !> @ingroup laser_params
    REAL(wp)  :: euler_gamma(max_sols) = 0.0_wp

    ! Secondary (XUV) Pulse:

    !> If Ellipticity_XUV is not set in the input.conf file, the XUV pulse is
    !> is linearly polarized in the z direction
    !> Ellipticity_XUV = +1 gives circ. polarized in the zy plane with E x dE/dt pointing to +x
    !> Ellipticity_XUV = -1 gives circ. polarized in the zy plane with E x dE/dt pointing to -x
    !> Ellipticity_XUV = 0  gives linearly polarized in the z-direction
    !> unless Cross_Polarized = True in which case
    !> the XUV pulse is linearly polarized in the y-direction
    !> @ingroup laser_params
    REAL(wp)  :: ellipticity_XUV(max_sols) = 0.0_wp

    !> Cross_Polarized determines if the secondary field is to be arbitrarily
    !> polarized (which it is by default) or polarized at right angles to the
    !> primary field (cross_polarized = .true.)
    !> Note that setting (cross_polarized  = .true.) overrides the setting of
    !> Ellipticity, such that both the primary and secondary pulses are linearly
    !> polarized.
    !! @ingroup laser_params
    LOGICAL   :: cross_polarized(max_sols) = .false.

    !> Frequency of the XUV pulse in a.u.
    !! @ingroup laser_params
    REAL(wp)  :: frequency_XUV(max_sols) = 1.0_wp
    !> Sets both the ramp on and off
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_ramp_on_XUV(max_sols) = 2.5_wp
    !> Total number of field cycles including the ramp on and off
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_pulse_XUV(max_sols) = 5.0_wp
    !> The carrier envelope phase measured in degrees
    !! @ingroup laser_params
    REAL(wp)  :: ceo_phase_deg_xuv(max_sols) = 0.0_wp
    !> Peak intensity of the XUV pulse in units of 10**14 Wcm^-2
    !! @ingroup laser_params
    REAL(wp)  :: intensity_XUV(max_sols) = 1.0_wp

    !> The default plane can be changed to the xy plane using this parameter.
    !> This may be desirable, particularly for circularly polarized laser pulses,
    !> as it restricts the included symmetries to the dipole accessible states,
    !> which is reduced in the xy plane compared to the yz. The number of outer
    !> region channels is also approximately halved due to the reduction in the
    !> number of magnetic sublevels.
    !! @ingroup laser_params
    LOGICAL   :: xy_plane_desired(max_sols) = .false.

    !> The time between the peak of the primary and secondary pulses
    !> measured in femtoseconds.
    !> Negative delay corresponds to the secondary pulse peak arriving first.
    !> Delay between fields is only used if use_2colour_field = .true. above
    !! @ingroup laser_params
    REAL(wp)  :: time_between_peaks_in_fs(max_sols) = 0.0_wp


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !       CALCULATION INFORMATION         !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !> Output the expectation value of the dipole (length and velocity) operators
    !! on each time-step. Used for calculating high-harmonic and absorption spectra.
    !! @ingroup calc_params
    LOGICAL   :: dipole_output_desired = .false.
    !> Output the expectation value of the dipole velocity operator at each time-step.
    !! @ingroup calc_params
    LOGICAL   :: dipole_velocity_output = .false.
    !>Choose X, Y or Z dipole expectation values. Default is Z.
    LOGICAL :: dipole_dimensions_desired(1:3)  = (/ .FALSE.,.FALSE.,.TRUE./)

    !> Include long range interactions between the laser field and residual ion.
    !> @ingroup calc_params
    LOGICAL   :: wd_ham_interactions = .true.
    !> Include long range interactions between the ionised electron and residual ion.
    !> @ingroup calc_params
    LOGICAL   :: wp_ham_interactions = .true.
    !> Include long range interactions between the laser field and ejected electron.
    !> @ingroup calc_params
    LOGICAL   :: we_ham_interactions = .true.

    !>Sets initial wavefunction to that in the data directory, irrespective of
    !> wether a restart from a checkpoint is required. Default is false.
    LOGICAL   :: read_initial_wavefunction_from_file=.false.

    ! Absorbing boundary parameters:

    !> Absorb_Desired determines whether the outgoing wavefunction will be
    !> suppressed at some radius in order to negate reflections from an
    !> insufficiently large outer region boundary. Default is .false.
    !> @ingroup absorb
    LOGICAL   :: absorb_desired = .false.
    !> Sigma factor determines the severity of the absorption. It is the Gaussian
    !> RMS width. A small sigma gives a more gradual absorption. Default is 0.2
    !> @ingroup absorb
    REAL(wp)  :: sigma_factor = 0.2_wp
    !> The fraction of the outer region box at which the Gaussian mask is applied.
    !> The default value of 0.7 means the mask starts 70% of the way into the
    !> outer region
    !> @ingroup absorb
    REAL(wp)  :: start_factor = 0.7_wp
    !> Number of time steps between Absorptions at the boundary.
    !> This strongly influences the strength of Absorption.
    !> If Delta_t is made smaller, get more absorption. Default is 20
    !> @ingroup absorb
    INTEGER   :: absorption_interval = 20

    ! Parameters for windowing the dipole length expectation values

    !> If `window_harmonics == .true.` then a Gaussian mask is applied to the
    !> WP matrix so that the expectation value of the dipole is not dominated by
    !> contributions at large R (that wouldn't really contribute to harmonic
    !> generation. The position and shape of the mask are controlled by
    !> `window_cutoff` and `window_FWHM`.
    !> @ingroup calc_params
    LOGICAL :: window_harmonics = .true.
    !> The gaussian mask applied to the WP matrix starts at a radial coordinate
    !> `window_cutoff`.
    !> @ingroup calc_params
    REAL(wp) :: window_cutoff = 100.00_wp
    !> The gaussian mask applied to the WP matrix decays with a FWHM of
    !> `window_FWHM`.
    !> @ingroup calc_params
    REAL(wp) :: window_FWHM = 50.00_wp


    ! Define how many processors to work in each of the inner and outer regions:
    ! No_Of_PEs_to_Use_Inner must be greater than the number of L Blocks
    ! but no wper needs to be an exact multuple
    ! For ML=0, this is L_Max+1, for ML=1 this is approx 2*L_Max

    !> Defines the number of cores to be used for the inner region calculation.
    !! This must be greater than or equal to the number of inner region
    !! Hamiltonian blocks. In the most general atomic case, this will
    !! be given by 2(L_max +1)^2.
    !! @ingroup required_params
    INTEGER   :: no_of_pes_to_use_inner = -1
    !> The number of cores to be used for the outer region calculation.
    !! @ingroup required_params
    INTEGER   :: no_of_pes_to_use_outer = -1
    !> The number of OpenMP threads to employ in the inner region
    !! @ingroup calc_params
    INTEGER   :: no_of_omp_threads_inner = 1
    !> The number of OpenMP threads to employ in the outer region
    !! @ingroup calc_params
    INTEGER   :: no_of_omp_threads_outer = 1

    ! Define the number of points per outer region processor:
    ! X_Last_Master should be less because of work needed to process inner region data
    ! X_Last_Others and X_Last_Master must both be multiples of 4
    ! In both cases, X_Last must be at least 2*Propagation_Order

    !> The number of grid points allocated to the outer-region master
    !!(the first outer region core.) Due to the finite difference method employed,
    !! this value must be a multiple of four greater than or equal to twice
    !! the taylors_order. To achieve good load balancing this value should
    !! usually be chosen to be less than x_last_others as the outer-region
    !! master has additional work to perform.
    !! @ingroup required_params
    INTEGER   :: x_last_master = -1
    !> The number of grid points per outer region core. Due to the finite difference
    !! method employed, this value must be a multiple for four greater than or equal to
    !! twice the value of taylors_order.
    !! @ingroup required_params
    INTEGER   :: x_last_others = -1

    !> Choose the order of the Arnoldi propagator (Taylor not yet implemented)
    INTEGER   :: propagation_order = 8

    ! Time step parameters:
    ! Steps_per_run is defined in calculation_parameters to
    ! ensure that it is a multiple of the various Checkpoint/Output parameters.
    ! delta_t is defined as well in calculation parameters

    !> Defines the number of time-steps to be executed in the propagation.
    !> The actual number of time-steps will depend on the checkpointing conditions
    !! @ingroup required_params
    INTEGER   :: steps_per_run_approx = -1
    !> Defines the time, in atomic units, up to which the solution should be
    !> propagated. Implicitly, then, the time-step for the propagation is defined by
    !> delta_t = final_t / steps_per_run_approx, and should be ensured that this is
    !> sufficiently small to achieve convergence of the result. In practice we have
    !> found that a time-step of 0.01 a.u. gives good results for most applications.
    !! @ingroup required_params
    REAL(wp)  :: final_t = -1.00_wp ! Time in atomic units

    !> Defines the outer-region grid spacing in atomic units.
    !> Numerical stability requires a small DeltaR.
    !> Note: size of outer region determined by the product:
    !> box_size = (no_of_pes_to_use_outer -1)* deltaR * x_last_others +
    !> deltaR * x_last_master.
    !! @ingroup required_params
    REAL(wp)  :: deltaR = -1.0_wp
 
    !> A complete dump of all output data will be performed at every checkpoint,
    !> from which the calculation may be restarted.
    !> @ingroup calc_params
    INTEGER   :: checkpts_per_run = 1
    !> The interval at which output data is written to file.
    !> Note that the electric field and dipole data are written at every timestep
    !> if dipole_output_desired is true.
    !! @ingroup calc_params
    INTEGER   :: timesteps_per_output = 20

    ! Used to get number of highest eigenvalues removed from each symmetry block.
    ! A number of states need to be removed in order to maintain stability in the
    ! norm of the time-dependent wavefunctions.
    !
    ! States that need to be retained will have large contributions
    ! to the surface amplitude, typically of the order 0.1-1.0, whereas states
    ! with surface amplitudes of the order 1.0E-10 can be safely removed
    ! This is due to the Bloch operator, which leads to high energies for states
    ! with noticeable boundary amplitudes

    ! If Remove_Eigvals_Threshold is set to .true. then high lying eigenvalues
    ! with surface amplitudes > Surf_Amp_Threshold_Value are retained (recommended)

    ! If Remove_Eigvals_Threshold is set to .false. then the first symmetry block
    ! will have the last neigsrem_1st_sym eigenstates removed and the remaining
    ! symmetry blocks will have neigsrem_higher_syms eigenstates removed.

    !> Removes highest energy eigenvalues from each state to ensure stability of norm.
    !> @ingroup target_info
    LOGICAL   :: remove_eigvals_threshold = .true.
    !> Eigenvalues with surface amplitudes lower than `surf_amp_threshold_value` are discarded.
    !> @ingroup target_info
    REAL(wp)  :: surf_amp_threshold_value = 1.0_wp
    !> Removes the last `neigsrem_1st_sym` eigenstates from the 1st symmetry block to ensure
    !> stability of the norm.
    !> @ingroup target_info
    INTEGER   :: neigsrem_1st_sym = 14
    !> Removes `neigsrem_higher_syms` states from all remaining symmetry blocks.
    !> @ingroup target_info
    INTEGER   :: neigsrem_higher_syms = 20

    !> Order of the time propagator to employ
    !! @ingroup calc_params
    INTEGER   :: taylors_order = 8

    !> Determines whether the detailed internal timing information is output by
    !> the inner/outer region masters and the first non-master outer region core
    !! @ingroup calc_params
    LOGICAL   :: timings_desired = .false.
    !> Retains all previously output checkpoint data if set to true.
    !> This can be used to track the evolution of the wavefunction (for instance).
    !> @ingroup calc_params
    LOGICAL   :: keep_checkpoints = .false.
    !> Write extra debugging information to the screen at run-time.
    !> @ingroup calc_params
    LOGICAL   :: debug = .false.
    !> Output the population data as a single unformatted file.
    !> Saves substantil execution time for large calculations.
    !> @ingroup calc_params
    LOGICAL   :: binary_data_files = .false.
    !> Write the ground state wavefunction to file.
    !> @ingroup calc_params
    LOGICAL   :: write_ground = .true.
    !> Store the outer region population per channel
    !> @ingroup calc_params
    LOGICAL :: keep_popL = .true.


    !> Tolerance for check of hermitian dipole. If the imaginary
    !> component of the dipole expectation value is larger than
    !> this value, RMT will raise an error
    Real(kind=wp) :: Hermitian_Tolerance = 10e-12_wp

    !> Version identifier to suffix to all output files
    !> The final suffix also contains an intensity identifier
    !! @ingroup calc_params
    CHARACTER(LEN=18)  :: version_root = ''

    ! Derived parameters:
    INTEGER   :: no_of_pes_to_use
    REAL(wp)  :: field_period_XUV(max_sols)
    REAL(wp)  :: ceo_phase_rad_xuv(max_sols)
    REAL(wp)  :: ceo_phase_rad(max_sols)
    REAL(wp)  :: field_period(max_sols)
    REAL(wp)  :: time_between_IR_and_XUV_peaks(max_sols)
    REAL(wp)  :: peak_time_of_IR_minus_ramp(max_sols)
    REAL(wp)  :: delay_XUV_in_IR_periods(max_sols)
    REAL(wp)  :: delay_between_IR_fields(max_sols)

    namelist /InputData/ ellipticity, frequency, lplusp, ML_max, gs_finast,&
    &adjust_gs_energy, gs_energy_desired, use_2colour_field, use_leakage_pulse,&
    &periods_of_ramp_on, periods_of_pulse, ceo_phase_deg, intensity, intensity2ir,&
    &frequency_xuv, periods_of_ramp_on_xuv, periods_of_pulse_xuv, ceo_phase_deg_xuv,&
    &intensity_xuv, time_between_peaks_in_fs, dipole_output_desired,&
    &dipole_velocity_output, no_of_pes_to_use_inner, no_of_pes_to_use_outer,&
    &no_of_omp_threads_inner, no_of_omp_threads_outer, x_last_master, x_last_others,&
    &taylors_order, dipole_format_id, steps_per_run_approx, final_t, deltar,&
    &version_root, keep_checkpoints, checkpts_per_run, no_of_field_confs, use_field_from_file,&
    &timesteps_per_output, remove_eigvals_threshold, surf_amp_threshold_value,&
    &neigsrem_1st_sym, neigsrem_higher_syms, ellipticity_xuv, xy_plane_desired,&
    &cross_polarized, euler_alpha, euler_beta, euler_gamma, wd_ham_interactions,&
    &wp_ham_interactions, we_ham_interactions, absorb_desired, sigma_factor,&
    &start_factor, absorption_interval, molecular_target, debug, write_ground,&
    &binary_data_files, window_harmonics, window_cutoff, window_FWHM, coupling_id,&
    &dipole_dimensions_desired, read_initial_wavefunction_from_file, timings_desired, &
    &Hermitian_Tolerance, keep_popL


    CHARACTER(LEN=:), ALLOCATABLE    :: disk_path

CONTAINS

    SUBROUTINE get_disk_path(default_path)

        CHARACTER(LEN=*), INTENT(IN) :: default_path
        CHARACTER(LEN=80)            :: read_path
        INTEGER                      :: disk_path_length
        INTEGER                      :: ierr

        CALL get_environment_variable('disk_path', read_path, disk_path_length, ierr, .true.)

        IF (disk_path_length == 0) THEN
            disk_path = default_path
        ELSE
            ALLOCATE (CHARACTER(disk_path_length) :: disk_path)
            disk_path = read_path(1:disk_path_length)
        END IF

    END SUBROUTINE get_disk_path

!---------------------------------------------------------------------------

    SUBROUTINE set_defaults

        no_of_field_confs        = 1
        molecular_target         = .false.
        ellipticity              = 0.0_wp
        euler_alpha              = 0.0_wp
        euler_beta               = 0.0_wp
        euler_gamma              = 0.0_wp
        frequency                = 1.0_wp
        lplusp                   = 0
        set_ML_max               = .true.
        ML_max                   = -1
        gs_finast                = 1
        adjust_gs_energy         = .false.
        gs_energy_desired        = 0.0_wp
        use_2colour_field        = .false.
        use_leakage_pulse        = .false.
        use_field_from_file      = .false.
        periods_of_ramp_on       = 2.5_wp
        periods_of_pulse         = 5.0_wp
        ceo_phase_deg            = 0.0_wp
        intensity                = 1.0_wp
        intensity2IR             = 0.0_wp
        ellipticity_XUV          = 0.0_wp
        cross_polarized          = .false.
        frequency_XUV            = 1.0_wp
        periods_of_ramp_on_XUV   = 2.5_wp
        periods_of_pulse_XUV     = 5.0_wp
        ceo_phase_deg_xuv        = 0.0_wp
        intensity_XUV            = 1.0_wp
        time_between_peaks_in_fs = 0.0_wp
        xy_plane_desired         = .false.
        dipole_output_desired    = .false.
        dipole_velocity_output   = .false.
        no_of_pes_to_use_inner   = -1
        no_of_pes_to_use_outer   = -1
        no_of_omp_threads_inner  = 1
        no_of_omp_threads_outer  = 1
        x_last_master            = -1
        x_last_others            = -1
        taylors_order            = 8
        steps_per_run_approx     = -1
        final_t                  = -1.0_wp
        deltaR                   = -1.0_wp
        version_root             = ''
        keep_checkpoints         = .false.
        checkpts_per_run         = 1
        timesteps_per_output     = 20
        timings_desired          = .false.
        remove_eigvals_threshold = .true.
        surf_amp_threshold_value = 1.0_wp
        neigsrem_1st_sym         = 14
        neigsrem_higher_syms     = 20
        wd_ham_interactions      = .true.
        wp_ham_interactions      = .true.
        we_ham_interactions      = .true.
        dipole_format_id         = RMatrixII_format_id
        absorb_desired           = .false.
        sigma_factor             = 0.2_wp
        start_factor             = 0.7_wp
        absorption_interval      = 20
        debug                    = .false.
        coupling_id              = LS_coupling_id
        write_ground             = .true.
        binary_data_files        = .false.
        window_harmonics         = .true.
        read_initial_wavefunction_from_file = .false.
        window_cutoff            = 100.00_wp
        window_FWHM              = 50.00_wp
        Hermitian_Tolerance      = 10e-12_wp
        keep_popL               = .true.
        dipole_dimensions_desired=(/.FALSE.,.FALSE.,.TRUE./)

    END SUBROUTINE

!---------------------------------------------------------------------------

    SUBROUTINE read_initial_conditions
    ! Subroutine to read calculation parameters from input.conf

        USE rmt_assert, ONLY: assert

        IMPLICIT NONE

        INTEGER           :: ioerror, numsols
        CHARACTER(LEN=80) :: line

        CALL set_defaults

        ! Try reading pure namelist format
        OPEN (UNIT=10, FILE=disk_path//'input.conf', STATUS='old')
        READ (10, NML=inputdata, IOSTAT=ioerror)
        IF (IOerror/=0) THEN
            BACKSPACE(10)
            READ(10,FMT='(A)') line
            CALL assert(.false.,'Invalid parameter in input.conf: '//TRIM(line))
        END IF
        CLOSE (10)

        CALL assert( (no_of_pes_to_use_inner /= -1) , &
        'Error: there is no no_of_pes_to_use_inner entry in input file')

        CALL assert( (no_of_pes_to_use_outer /= -1) , &
        'Error: there is no no_of_pes_to_use_outer entry in input file')

        CALL assert( (x_last_master /= -1) , &
        'Error: there is no x_last_master entry in input file')

        CALL assert( (x_last_others /= -1) , &
        'Error: there is no x_last_others entry in input file')

        CALL assert( (steps_per_run_approx /= -1) , &
        'Error: there is no steps_per_run_approx entry in input file')

        CALL assert( (final_t /= -1) , &
        'Error: there is no final_t entry in input file')

        CALL assert( (deltar /= -1) , &
        'Error: there is no deltar entry in input file')

        ! abbreviation
        numsols = no_of_field_confs

        ! Derived parameters
        no_of_pes_to_use = no_of_pes_to_use_inner + no_of_pes_to_use_outer
        field_period_XUV(1:numsols) = 2.0*pi/frequency_XUV(1:numsols)
        ceo_phase_rad_xuv(1:numsols) = pi*ceo_phase_deg_xuv(1:numsols)/180.0_wp
        ceo_phase_rad(1:numsols) = pi*ceo_phase_deg(1:numsols)/180.0_wp
        field_period(1:numsols) = 2.0*pi/frequency(1:numsols)

        time_between_IR_and_XUV_peaks(1:numsols) = time_between_peaks_in_fs(1:numsols)*41.341373_wp
        peak_time_of_IR_minus_ramp(1:numsols) = &
            periods_of_pulse(1:numsols) / (2.0_wp*frequency(1:numsols)) - &
            periods_of_pulse_XUV(1:numsols)/(frequency_XUV(1:numsols)*2.0_wp)
        delay_XUV_in_IR_periods(1:numsols) = &
            ((time_between_IR_and_XUV_peaks(1:numsols)/(2.0_wp*pi)) + &
             peak_time_of_IR_minus_ramp(1:numsols)) * frequency(1:numsols)

        delay_between_IR_fields(1:numsols) = &
            (time_between_IR_and_XUV_peaks(1:numsols)/(2.0_wp*pi)) &
            *frequency(1:numsols)

        WHERE (cross_polarized(1:numsols)) ellipticity(1:numsols) = 0

        IF (ML_max == -1) set_ML_max = .false.

        ! To rotate from the default yz plane to xy plane, the Euler angles should all be set to 90 degrees.
        ! This means ex becomes ez,  ey becomes -ey, ez becomes ex.
        ! So the default polarization vector (z-i*Ellipticity*y) becomes (x+i*Ellipticity*y)
     
        CALL assert(.NOT.(ANY(xy_plane_desired(1:numsols)) .AND. set_ML_max), &
        "Both ML max and xy plane polarization set. Choose only one of these.")

        CALL assert(.NOT.(ANY(xy_plane_desired(1:numsols)) .AND. (GS_finast /= 1)),&
        "xy-plane reduction can only be used for S^e initial state")

        WHERE (xy_plane_desired(1:numsols))
           euler_alpha(1:numsols) = 90
           euler_beta(1:numsols)  = 90
           euler_gamma(1:numsols) = 90
        END WHERE

    END SUBROUTINE read_initial_conditions

END MODULE initial_conditions

!> @defgroup required_params Required Parameters
!> @defgroup target_info Target Informatiom
!> @defgroup laser_params Laser Parameters
!> @defgroup calc_params Calculation Parameters
!> @defgroup absorb Absorbing Boundary Parameters

!> @page inputs RMT: Input
!!
!! @brief Description of input files and parameters for RMT
!!
!! @tableofcontents
!!
!!
!!
!! @section data_files Data files
!!
!! The input data containing information about the ground and excited states of the
!! target system, the dipole couplings and the spline basis is produced by one of
!! three different software packages.  Non-relativistic atomic data can be provided
!! by either the R-matrix I [1](http://connorb.freeshell.org) or R-matrix II
!! packages [2](https://gitlab.com/Uk-amor/RMT/rmatrixII).  Semi-relativistic data
!! is produced by R-matrix I, and molecular data by UKRMol+
!! [3](https://gitlab.com/Uk-amor/UKRMol), all of which can be obtained, along with
!! documentation, at the repositories linked.
!!
!! The input files required are slightly different depending on the package used.
!! For molecular calculations, all relevant input data is stored in a single file
!! `molecular_data`.  For both the atomic cases, the Hamiltonian data is stored in
!! file `H` and the spline basis information in files `Splinedata` and
!! `Splinewaves`.  The dipole information is stored in files `d` and `d00` for
!! R-matrix II.  For R-matrix I, an individual file is used for each dipole
!! coupling block.  Hence the header information is stored in file `D00` and the
!! individual dipole files are of the form `D###` (e.g. `D001`, `D002` etc.)
!!
!!
!! @section input_params Input parameters
!!
!! RMT reads input from the file ‘input.conf’ which controls the setup of the laser
!! pulse(s), the parallel structure of the calculation and various other
!! calculation specific parameters. Of these parameters, some are required, and
!! others can be left to default values. The list below contains all the input
!! parameters which can be set, along with their default values.
!!
!! Lines in the input file may be commented with the hash symbol `#`. All
!! inputs begin with the option name (All options are case sensitive) then
!! an equals sign, followed by the value of the option. for example:
!!
!! `Frequency  = 2.0`
!!
!! true/false options can be written with or without the fortran dots, for
!! example
!!
!! `USE_2Colour_Field          = false`
!!
!! `Keep_CheckPoints           =.false.`
!!
!! are both valid.
!!
!! all spaces are stripped from the file, but tab characters will break the
!! input.
!!
!! There is no need to include quotation marks for strings e.g:
!!
!! `Version_root               = He_w390_I101014_05`
!!
!! @subsection req_params Required parameters
!!
!!
!! * @ref required_params "Required Parameters"
!!
!! @subsection opt_params Optional Parameters
!!
!! @subsubsection targ_inf Target Information
!!
!! * @ref target_info "Target Informtaion"
!!
!! For most purposes the default values of these parameters are advised.
!!
!! @subsubsection efield Electric Field Parameters
!!
!!
!! * @ref laser_params "Laser Parameters"
!!
!!
!! @subsubsection cal_pars Calculation parameters
!!
!!
!! * @ref calc_params "Calculation Parameters"
!!
!! @subsubsection add_params Additional parameters
!!
!! @paragraph disk_path Disk Path
!!
!! By default, the executable will look in the run-time directory
!! for the input files, `input.conf`, `H`, `d00` etc.
!! Should you want to place the input files in a different
!! location, for instance for a batch job, the path to the input
!! files should be set as the environment variable `disk_path`.
!!
!! e.g. batch job script for queuing system:
!!
!! ```
!! >> ls
!!
!!    submit.pbs run_dir_1 run_dir_2
!!
!! >> ls run_dir*
!!
!!    run_dir_1:
!!
!!    d d00 data ground H input.conf rmt.x Splinedata Splinewaves state
!!
!!    run_dir_2:
!!
!!    d d00 data ground H input.conf rmt.x Splinedata Splinewaves state
!!
!! >> tail submit.pbs
!!
!!    # variable PBS_ARRAY_INDEX loops over values 1,2
!!
!!    export disk_path=$PWD/run_dir_$PBS_ARRAY_INDEX
!!
!!    mpirun -n 32 $disk_path/rmt.x > $disk_path/rmt.log
!!
!! ```
!!
!! @paragraph abs_bound Absorbing boundary
!!
!! By default, no absorbing boundary is enabled in the outer
!! region. This means if your outer region is not sufficiently
!! large the outgoing wavefunction will reflect from the boundary
!! and create spurious oscillations in the results.
!! Enabling the absorbing boundary is not possible at run-time, as
!! it is hard-coded in the module `calculation_parmaters`.
!!
!! There are four relevant variables therein:  @ref absorb "Absorbing Boundary Parameters"
!!
!!    `Absorb_Desired` (Logical- set true to enable)
!!    `Start_Factor`. `Start_Factor=0.7` would start the absorbing
!!    boundary 70% in to the outer region. `Sigma_Factor` sets the severity of the absorption. the larger
!!    this is, the gentler the absorption.
!!
!!   The equation for the mask function is given by
!!    \f{eqnarray*}{
!!    M(x) & = 1 \quad \mbox{ for } x< x_s \\ \\
!!    M(x) &= exp\left[-\left(\frac{x-x_s}{\sigma}\right)^2\right]
!!    \quad \mbox{ for } x_s \le x \le x_l
!!    \f}
!!
!!
!! where \f$x_l\f$ is the outermost point in the outer
!! region, \f$x_s =\f$
!! `start_factor`\f$\;\times\; x_l\f$ and
!! \f$\sigma=\frac{x_l}{2}\;\times\;\f$`sigma_factor`
!!
!! The mask function for an outer region of 1000 a.u. is shown for a
!! variety of parameters below. Note that the outermost point is 1020
!! a.u. as the inner region is 20 a.u.
!!
!! @image html absorb.png
!! @image latex absorb.png
