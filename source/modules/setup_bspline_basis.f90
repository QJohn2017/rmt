! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up the spline basis to be used in the inner region.

MODULE setup_bspline_basis

    USE precisn,    ONLY: wp
    USE rmt_assert, ONLY: assert

    IMPLICIT NONE

    REAL(wp), ALLOCATABLE, SAVE       :: bsplines(:, :)

    PUBLIC setup_IR_basis_for_fd_points, dealloc_R_basis_for_fd_points
    PUBLIC bsplines

CONTAINS

    SUBROUTINE setup_IR_basis_for_fd_points(dr, nfdm)

        USE readhd,  ONLY: bspl_k9, &
                           bspl_nb, &
                           bspl_nt, &
                           bspl_t, &
                           rmatr
        USE splines, ONLY: bsplvb, interv
        USE initial_conditions, ONLY : debug

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: nfdm
        INTEGER                     :: i, err
        INTEGER                     :: left, leftmk, mflag
        REAL(wp), INTENT(IN)        :: dr
        REAL(wp)                    :: spoint, xt
        REAL(wp), ALLOCATABLE       :: r(:), values(:)

        ALLOCATE (bsplines(nfdm + 1, bspl_nb), values(bspl_nb), r(nfdm), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with bspline data')

        spoint = rmatr - (REAL(nfdm, wp)*dr)
        r(1) = spoint

        DO i = 2, nfdm
            r(i) = r(i - 1) + dr
        END DO

        IF (debug) THEN
            PRINT *, 'grid points in the inner region: '
            DO i = 1, nfdm
                PRINT *, r(i)
            END DO
        END IF

        DO i = 1, nfdm
            values = 0.0_wp
            xt = r(i)
            CALL interv(bspl_t, bspl_nb, xt, left, mflag)
            leftmk = left - bspl_k9
            CALL bsplvb(bspl_t, bspl_nt, bspl_k9, 1, xt, left, values(leftmk + 1))
            bsplines(i, :) = values(:)
        END DO

        DEALLOCATE (values, r, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with bspline data')

    END SUBROUTINE setup_IR_basis_for_fd_points

!---------------------------------------------------------------------------

    SUBROUTINE dealloc_R_basis_for_fd_points

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (bsplines, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with bspline data')

    END SUBROUTINE dealloc_R_basis_for_fd_points

END MODULE setup_bspline_basis
