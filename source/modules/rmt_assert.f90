! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Checks the truth of a statement and exits gracefully if false.
MODULE rmt_assert

    USE MPI, ONLY: MPI_COMM_WORLD, MPI_ABORT

    IMPLICIT NONE

CONTAINS

    ! Terminate MPI: always ABORT says Cray.
    SUBROUTINE assert(this_must_be_true, error_message)

        LOGICAL, INTENT(IN)                 :: this_must_be_true
        CHARACTER(*), INTENT(IN), OPTIONAL  :: error_message

        INTEGER, PARAMETER :: errorcode = 2 ! outputs this number upon abort
        INTEGER            :: err

        IF (this_must_be_true .EQV. .false.) THEN

            IF (PRESENT(error_message)) THEN
                PRINT *, error_message
            END IF

            CALL MPI_ABORT(MPI_COMM_WORLD, errorcode, err)

        END IF

    END SUBROUTINE assert

END MODULE rmt_assert
