! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief High level controller for wavefunction data setup and tear down (routines
!> used only at start and end of calculation).
MODULE setup_wv_data

    IMPLICIT NONE

    PUBLIC setup_inner_region_wv
    PUBLIC deallocate_distribute_wv_data

CONTAINS

    SUBROUTINE setup_inner_region_wv (i_am_inner_master, i_am_block_master, nfdm, my_post)

        USE distribute_wv_data,  ONLY: setup_and_distribute_wv_data
        USE initial_conditions,  ONLY: molecular_target
        USE readhd,              ONLY: inner_master_reads_spline_files, &
                                       deallocate_read_spline_files
        USE setup_bspline_basis, ONLY: dealloc_R_basis_for_fd_points

        LOGICAL, INTENT(IN)  :: i_am_inner_master
        LOGICAL, INTENT(IN)  :: i_am_block_master
        INTEGER, INTENT(IN)  :: nfdm
        INTEGER, INTENT(OUT) :: my_post

        IF (i_am_inner_master .AND. .NOT. molecular_target) THEN
            CALL inner_master_reads_spline_files
            CALL inner_master_precomputes_ib_amplitudes
        END IF

        CALL setup_and_distribute_wv_data (i_am_inner_master, i_am_block_master, nfdm, my_post)

        IF (i_am_inner_master) THEN
            CALL deallocate_read_spline_files
            IF (.NOT. molecular_target) THEN
                CALL dealloc_R_basis_for_fd_points
            END IF
        END IF

    END SUBROUTINE setup_inner_region_wv

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_distribute_wv_data

        USE distribute_wv_data, ONLY: dealloc_distribute_wv_data

        CALL dealloc_distribute_wv_data

    END SUBROUTINE deallocate_distribute_wv_data

!---------------------------------------------------------------------------

    !> \brief   Evaluate wave function at inside-boundary points
    !> \authors J Benda
    !> \date    2018
    !>
    !> This function is called only after both the H and Spline* files are read in. It evaluates the wave function
    !> amplitudes at inside-boundary points and stores them in the array wamp2. Only used in atomic mode, as otherwise
    !> the array wamp2 is populated already in the subroutine \ref read_molecular_data.
    !>
    SUBROUTINE inner_master_precomputes_ib_amplitudes

        USE calculation_parameters, ONLY: nfdm
        USE precisn,                ONLY: wp
        USE readhd,                 ONLY: no_Of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, &
                                          L_block_nchan, L_block_post, &
                                          bspl_ndim, splwave, wamp2, finind_L, nchmx, nstmx, inast
        USE rmt_assert,             ONLY: assert
        USE setup_bspline_basis,    ONLY: bsplines

        INTEGER     :: i, j, k, ii, jj, iik, symind, err
        REAL(wp)    :: sm

        ALLOCATE (wamp2(nchmx,nstmx,nfdm,inast), stat = err)
        CALL assert (err == 0, 'Inner_Master_Precomputes_IB_Amplitudes: Memory allocation of wamp2 failed.')

        DO j = 1, no_Of_LML_blocks
            CALL finind_L (LML_block_nspn(j), LML_block_lrgl(j), LML_block_npty(j), symind)
            DO k = 1, L_block_nchan(symind)
                iik = 0
                DO ii = L_block_post(symind-1)+1, L_block_post(symind)
                    iik = iik + 1
                    DO i = 1,nfdm
                        sm = 0
                        DO jj = 1, bspl_ndim
                            sm = sm + splwave((k-1)*bspl_ndim+jj,ii-L_block_post(symind-1),symind) * bsplines(i,jj+1)
                        END DO
                        wamp2(k,iik,i,symind) = sm
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE inner_master_precomputes_ib_amplitudes

END MODULE setup_wv_data
