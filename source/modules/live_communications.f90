! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Performs parallel operations in inner-region. Sets up links
!> between dipole coupled symmetry blocks, and contains routines for performing
!> the hamiltonian-wavefunction multiplication operation in parallel across the multilayer
!> parallelised blocks.

MODULE live_communications

    USE precisn,               ONLY: wp
    USE rmt_assert,            ONLY: assert
    USE distribute_hd_blocks2, ONLY: numrows, &
                                     mv_disp, &
                                     mv_counts, &
                                     loc_diag_els
    USE initial_conditions,    ONLY: numsols => no_of_field_confs
    USE mpi_communications,    ONLY: mpi_comm_region
    USE mpi_layer_lblocks,     ONLY: Lb_comm, &
                                     i_am_block_master
    USE readhd,                ONLY: max_L_block_size
    USE MPI

    IMPLICIT NONE

    INTEGER, SAVE :: no_vec1_init, no_vec2_init, no_vec3_init

    INTEGER, SAVE :: no_vecup, no_vecdown, no_vecsame

    COMPLEX(wp), ALLOCATABLE, SAVE :: my_vec1s(:, :, :), my_vec2s(:, :, :), my_vec3s(:, :, :)
    INTEGER, ALLOCATABLE, SAVE :: my_blocksize1s(:), my_blocksize2s(:), my_blocksize3s(:)

    INTEGER, SAVE :: ml  ! DDAC: TEMPORARY

    PUBLIC parallel_matrix_vector_multiply
    PUBLIC parallel_matrix_vector_multiply_no_field
    PUBLIC psi_Z_psi_multiplication_inner
    PUBLIC gram_schmidt_orthog_lanczos
    PUBLIC order0_pes_sum_vecs
    PUBLIC get_global_pop_inner
    PUBLIC sum_and_distribute_total_beta
    PUBLIC setup_block_links

CONTAINS

!-----------------------------------------------------------------------
! This routine should only be called by L block masters
!-----------------------------------------------------------------------

    SUBROUTINE setup_block_links ! Molecular

        USE mpi_layer_lblocks, ONLY: Lb_m_rank
        USE initial_conditions, ONLY : debug
        USE readhd,            ONLY: dipole_coupled, &
                                     nocoupling, &
                                     downcoupling, &
                                     samecoupling, &
                                     upcoupling, &
                                     no_of_LML_blocks

        IMPLICIT NONE

        INTEGER :: i, j
        INTEGER :: cnt, ivec1, ivec2, ivec3 ! GSJA: added


        ! Each task constructs a table containing the wavefunction symmetries
        ! that every block master needs to perform the matrix*vector multiplication.
        ! The table contains also the diagonal couplings (if present) which
        ! do not have to be sent accross to other processes.

        IF (i_am_block_master) THEN
            ivec1 = 0
            ivec2 = 0
            ivec3 = 0

            DO i = 1, no_of_LML_blocks !finast
                DO j = 1, no_of_LML_blocks !finast
                    cnt = dipole_coupled(j, i)
                    IF (cnt /= nocoupling) THEN
                        IF (i .NE. j .AND. debug) PRINT *, i, 'requires wf from Lb_m_rank', j - 1
                        ! GSJA: added count of no of vec(u/s/d)
                        IF (Lb_m_rank .EQ. j - 1) THEN
                            IF (IAND(cnt, upcoupling) /= 0) ivec1 = ivec1 + 1     !>Was originally Down
                            IF (IAND(cnt, downcoupling) /= 0) ivec2 = ivec2 + 1 !>was originally up
                            IF (IAND(cnt, samecoupling) /= 0) ivec3 = ivec3 + 1
                        END IF
                    END IF
                END DO
            END DO

            no_vecup = ivec1
            no_vecdown = ivec2
            no_vecsame = ivec3

            no_vec1_init = no_vecup
            no_vec2_init = no_vecdown
            no_vec3_init = no_vecsame

            CALL allocate_my_vecs  ! GSJA: added

            IF (debug) THEN
                WRITE (*, *) 'rank,no_vec1_init=', Lb_m_rank, no_vec1_init
                WRITE (*, *) 'rank,no_vec2_init=', Lb_m_rank, no_vec2_init
                WRITE (*, *) 'rank,no_vec3_init=', Lb_m_rank, no_vec3_init
            END IF

        END IF

    END SUBROUTINE setup_block_links

!-----------------------------------------------------------------------

    SUBROUTINE parallel_matrix_vector_multiply(field_real, vecin, vecout, t_step2, dipole_output, vecout2)

        USE distribute_hd_blocks2, ONLY: loc_dblock_u, &
                                         loc_dblock_d, &
                                         loc_dblock_s, &
                                         loc_vblock_u, &
                                         loc_vblock_d, &
                                         loc_vblock_s
        USE global_data,           ONLY: one

        IMPLICIT NONE

        REAL(wp),    INTENT(IN)              :: field_real(3, numsols)
        COMPLEX(wp), INTENT(IN)              :: vecin(numrows, numsols), t_step2
        COMPLEX(wp), INTENT(OUT)             :: vecout(numrows, numsols)
        COMPLEX(wp), INTENT(OUT), OPTIONAL   :: vecout2(numrows, numsols)
        LOGICAL, INTENT(IN), OPTIONAL        :: dipole_output
        LOGICAL                              :: dipole_velocity_output
        INTEGER                              :: ii, ierr, isol
        INTEGER                              :: n_up, n_down, n_same
        INTEGER                              :: blocksizeup_aux, blocksizedown_aux, blocksizesame_aux
        INTEGER                              :: blocksize_up(3), blocksize_down(3), blocksize_same(3)
        COMPLEX(wp)                          :: colvecin(max_L_block_size, numsols)
        COMPLEX(wp)                          :: vecup(max_L_block_size, numsols, 3), &
                                                vecdown(max_L_block_size, numsols, 3), &
                                                vecsame(max_L_block_size, numsols, 3)
        colvecin = (0.0_wp, 0.0_wp)

        ! Put vecin from all cores in this symmetry block into colvecin
        DO isol = 1, numsols
            CALL MPI_GATHERV(vecin(:, isol), numrows, MPI_DOUBLE_COMPLEX, colvecin(:, isol), &
                             mv_counts, mv_disp, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
        END DO

        ! Send/Recv colvecin to/from neighbouring L blocks
        IF (i_am_block_master) THEN
            CALL send_recv_master_vecs_ptp(colvecin, vecup, vecdown, vecsame, &
                                           blocksize_up, blocksize_down, blocksize_same, &
                                           n_up, n_down, n_same, field_real)
        END IF

        ! Calculate diagonal elements: not required for calculating dipole expectation
        IF (PRESENT(dipole_output))  THEN
            dipole_velocity_output = dipole_output
            vecout = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                vecout2 = (0.0_wp, 0.0_wp)
            END IF
        ELSE
            dipole_velocity_output = .FALSE.
            DO isol = 1, numsols
                 vecout(:, isol) = loc_diag_els(:) * vecin(:, isol) * t_step2
            END DO
        END IF

        ! All child tasks must know the length of the vectors received. If n_. > 0 then
        ! we need to use that vector to multiply with the corresponding dipole block.
        CALL MPI_BCAST(n_down, 1, MPI_INTEGER, 0, Lb_comm, ierr)
        CALL MPI_BCAST(n_same, 1, MPI_INTEGER, 0, Lb_comm, ierr)
        CALL MPI_BCAST(n_up, 1, MPI_INTEGER, 0, Lb_comm, ierr)

        ! If available use the u,d,s wavefunctions to multiply with the corresponding dipole blocks.
        IF (n_down > 0) THEN
            CALL MPI_BCAST(vecdown, n_down*numsols*max_L_block_size, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
            CALL MPI_BCAST(blocksize_down, n_down, MPI_INTEGER, 0, Lb_comm, ierr)
            DO ii = 1, n_down ! GSJA: added
                blocksizedown_aux = blocksize_down(ii)
                IF (numsols == 1) THEN
                    ! multiply matrix-vector in case of single solution
                    CALL ZGEMV('n', numrows, blocksizedown_aux, t_step2, loc_dblock_d, numrows, vecdown(:, 1, ii), &
                               1, one, vecout(:, 1), 1)
                    IF (dipole_velocity_output) THEN
                        CALL ZGEMV('n', numrows, blocksizedown_aux, t_step2, loc_vblock_d, numrows, vecdown(:, 1, ii), &
                                   1, one, vecout2(:, 1), 1)
                    END IF
                ELSE
                    ! multiply matrix-matrix in case of more solutions
                    CALL ZGEMM('n', 'n', numrows, numsols, blocksizedown_aux, t_step2, loc_dblock_d, numrows, &
                               vecdown(:, :, ii), max_L_block_size, one, vecout(:, :), numrows)
                    IF (dipole_velocity_output) THEN
                        CALL ZGEMM('n', 'n', numrows, numsols, blocksizedown_aux, t_step2, loc_vblock_d, numrows, &
                               vecdown(:, :, ii), max_L_block_size, one, vecout2(:, :), numrows)
                    END IF
                END IF
            END DO
        END IF

        IF (n_same > 0) THEN
            CALL MPI_BCAST(vecsame, n_same*numsols*max_L_block_size, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
            CALL MPI_BCAST(blocksize_same, n_same, MPI_INTEGER, 0, Lb_comm, ierr)
            DO ii = 1, n_same
                blocksizesame_aux = blocksize_same(ii)
                IF (numsols == 1) THEN
                    ! multiply matrix-vector in case of single solution
                    CALL ZGEMV('n', numrows, blocksizesame_aux, t_step2, loc_dblock_s, numrows, vecsame(:, 1, ii), &
                               1, one, vecout(:, 1), 1)
                    IF (dipole_velocity_output) THEN
                        CALL ZGEMV('n', numrows, blocksizesame_aux, t_step2, loc_vblock_s, numrows, vecsame(:, 1, ii), &
                                   1, one, vecout2(:, 1), 1)
                    END IF
                ELSE
                    ! multiply matrix-matrix in case of more solutions
                    CALL ZGEMM('n', 'n', numrows, numsols, blocksizesame_aux, t_step2, loc_dblock_s, numrows, &
                               vecsame(:, :, ii), max_L_block_size, one, vecout(:, :), numrows)
                    IF (dipole_velocity_output) THEN
                        CALL ZGEMM('n', 'n', numrows, numsols, blocksizesame_aux, t_step2, loc_vblock_s, numrows, &
                                   vecsame(:, :, ii), max_L_block_size, one, vecout2(:, :), numrows)
                    END IF
                END IF
            END DO
        END IF

        IF (n_up > 0) THEN
            CALL MPI_BCAST(vecup, n_up*numsols*max_L_block_size, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
            CALL MPI_BCAST(blocksize_up, n_up, MPI_INTEGER, 0, Lb_comm, ierr)
            DO ii = 1, n_up
                blocksizeup_aux = blocksize_up(ii)
                IF (numsols == 1) THEN
                    ! multiply matrix-vector in case of single solution
                    CALL ZGEMV('n', numrows, blocksizeup_aux, t_step2, loc_dblock_u, numrows, vecup(:, 1, ii), &
                               1, one, vecout(:, 1), 1)
                    IF (dipole_velocity_output) THEN
                        CALL ZGEMV('n', numrows, blocksizeup_aux, t_step2, loc_vblock_u, numrows, vecup(:, 1, ii), &
                                   1, one, vecout2(:, 1), 1)
                    END IF
                ELSE
                    ! multiply matrix-matrix in case of more solutions
                    CALL ZGEMM('n', 'n', numrows, numsols, blocksizeup_aux, t_step2, loc_dblock_u, numrows, &
                               vecup(:, :, ii), max_L_block_size, one, vecout(:, :), numrows)
                    IF (dipole_velocity_output) THEN
                        CALL ZGEMM('n', 'n', numrows, numsols, blocksizeup_aux, t_step2, loc_vblock_u, numrows, &
                                   vecup(:, :, ii), max_L_block_size, one, vecout2(:, :), numrows)
                    END IF
                END IF
            END DO
        END IF

    END SUBROUTINE parallel_matrix_vector_multiply

!-----------------------------------------------------------------------

    SUBROUTINE parallel_matrix_vector_multiply_no_field(vecin, vecout, t_step2)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(numrows, numsols), t_step2
        COMPLEX(wp), INTENT(OUT)    :: vecout(numrows, numsols)
        INTEGER                     :: isol

        ! Calculate diagonal elements
        DO isol = 1, numsols
            vecout(:, isol) = loc_diag_els(:) * vecin(:, isol) * t_step2
        END DO

    END SUBROUTINE parallel_matrix_vector_multiply_no_field

!-----------------------------------------------------------------------

    SUBROUTINE psi_Z_psi_multiplication_inner(psi, z_psi, expec_Z_inner)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)    :: psi(1:numrows, 1:numsols)
        COMPLEX(wp), INTENT(IN)    :: Z_psi(1:numrows, 1:numsols)
        COMPLEX(wp), INTENT(OUT)   :: expec_Z_inner(1:numsols)
        COMPLEX(wp), EXTERNAL      :: ZDOTC

        INTEGER :: isol

        DO isol = 1, numsols
            expec_Z_inner(isol) = ZDOTC(numrows, psi(:, isol), 1, Z_psi(:, isol), 1)
        END DO

    END SUBROUTINE psi_Z_psi_multiplication_inner

!-----------------------------------------------------------------------

    SUBROUTINE gram_schmidt_orthog_lanczos(jend, mx, vec1in, vec2in, qvecin, redmat)

        IMPLICIT NONE

        INTEGER,                  INTENT(IN)    :: jend, mx
        COMPLEX(wp),              INTENT(IN)    :: vec1in(numrows, numsols)
        COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: qvecin(:, :, :)  ! numrows x krydim x numsols
        COMPLEX(wp),              INTENT(INOUT) :: redmat(mx, mx, numsols), vec2in(numrows, numsols)

        INTEGER                     :: ierr, isol
        REAL(wp)                    :: hj1jb(numsols), tot_hj1jb(numsols)
        COMPLEX(wp)                 :: hijb(numsols), tot_hijb(numsols)
        COMPLEX(wp), EXTERNAL       :: ZDOTC ! BLAS routine

        DO isol = 1, numsols
            hijb(isol) = ZDOTC(numrows, vec1in(:, isol), 1, vec2in(:, isol), 1)
        END DO

        CALL MPI_ALLREDUCE(hijb, tot_hijb, numsols, MPI_DOUBLE_COMPLEX, MPI_SUM, mpi_comm_region, ierr)

        redmat(jend, jend, :) = tot_hijb(:)

        DO isol = 1, numsols
            IF (jend == 1) THEN
                CALL ZAXPY(numrows, -redmat(jend, jend, isol), vec1in(:, isol), 1, vec2in(:, isol), 1)
            ELSE
                CALL ZAXPY(numrows, -redmat(jend - 1, jend, isol), qvecin(1:numrows, jend - 1, isol), 1, vec2in(:, isol), 1)
                CALL ZAXPY(numrows, -redmat(jend, jend, isol), qvecin(1:numrows, jend, isol), 1, vec2in(:, isol), 1)
            END IF

            hj1jb(isol) = ZDOTC(numrows, vec2in(:, isol), 1, vec2in(:, isol), 1)
        END DO

        CALL MPI_ALLREDUCE(hj1jb, tot_hj1jb, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)

        tot_hj1jb(:) = SQRT(tot_hj1jb(:))

        IF (jend .LT. mx) THEN
            redmat(jend + 1, jend, :) = tot_hj1jb(:)
            redmat(jend, jend + 1, :) = tot_hj1jb(:)
        END IF

        DO isol = 1, numsols
            CALL ZDSCAL(numrows, 1.0_wp/tot_hj1jb(isol), vec2in(:, isol), 1)
        END DO

    END SUBROUTINE gram_schmidt_orthog_lanczos

!-----------------------------------------------------------------------

    !> \brief   Send a wave function segment from owner to other rank.
    !> \authors J Benda, A Brown
    !> \date    2018
    !>
    !> This SUBROUTINE sends a provided segment of the wave function owned by one of the MPI processes
    !> to another MPI process, and returns it along with the supplied component of the electric field. The sending
    !> and receiving can be done by the very same MPI process (i.e. i == j), without any danger of a deadlock.
    !> The send/recv pair is entirely non-blocking so all sends/recvs can be  performed without enforcing an order.
    !> In order to allow all send/recvs to complete before processing the received data the routine returns the
    !> request status of the send or recv to be passed to a MPI_Waitall.
    !>
    !> \param field         Component of the field to multiply the received vector with; used by 'j'.
    !> \param i             Owner of the wave function segment (one-based index of the total symmetry).
    !> \param my_vec        Wave function segment controlled by the current MPI process; referenced by process 'i'.
    !> \param j             Receiver of the wave function segment (one-based index of the total symmetry).
    !> \param n             Number of vectors in \c my_vecs and sizes in \c my_blocksizes; updated by 'j'.
    !> \param my_vecs       Collection of all wave function segments received by the process 'j'; updated by 'j'.
    !> \param my_blocksizes For all wave function segment in \c my_vecs, size of the segment.
    !> \param nvec          Maximal value of \c ivec, dimension of \c my_vecs and \c my_blocksizes.
    !> \param req           MPI request status for the non-blocking send or receive posted
    !> \param my_field      Updated vector of field components for processing received data after the MPI_Waitall
    !>
    SUBROUTINE send_recv_master_vecs_ptp_cmpt(field, i, my_vec, j, n, my_vecs, my_blocksizes, nvec, req, my_field)

        USE mpi_layer_lblocks, ONLY: Lb_m_rank, &
                                     Lb_m_comm
        USE readhd,            ONLY: states_per_LML_block

        IMPLICIT NONE

        INTEGER, INTENT(IN)        :: i, j, nvec
        INTEGER, INTENT(INOUT)     :: n
        INTEGER, INTENT(INOUT)     :: my_blocksizes(nvec)
        COMPLEX(wp), INTENT(IN)    :: field(numsols)
        COMPLEX(wp), INTENT(IN)    :: my_vec(max_L_block_size, numsols)
        COMPLEX(wp), INTENT(INOUT) :: my_vecs(max_L_block_size, numsols, nvec)
        INTEGER, INTENT(OUT)       :: req
        COMPLEX(wp), INTENT(INOUT) :: my_field(numsols, 3)

        INTEGER     :: ierr

        ! i-th master queues a non-blocking send of its (i-th) wave function segment
        IF (Lb_m_rank == i - 1) THEN
            CALL MPI_ISEND(my_vec, max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, j - 1, 0, Lb_m_comm, req, ierr)
        END IF

        ! j-th master queues a non-blocking receive for that segment into its
        ! buffer, and stores the relevant parameters for processing that data
        ! after the receive is complete
        IF (Lb_m_rank == j - 1) THEN
            n = n + 1
            CALL MPI_IRECV(my_vecs(:, :, n), max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, i - 1, 0, Lb_m_comm, req, ierr)
            my_blocksizes(n) = states_per_LML_block(i)
            my_field(:, n) = field(:)
        END IF

    END SUBROUTINE send_recv_master_vecs_ptp_cmpt

!-----------------------------------------------------------------------

    !> \brief   Communicate segments of the solution vector between processes
    !> \authors Z Masin, G Armstrong, J Benda, A Brown
    !> \date    2017 - 2018
    !>
    !> Block master 'j' needs the wave function segment from 'i' in case of non-zero coupling (j,i).
    !> The segment is stored in \c my_vec1s, \c my_vec2s or \c my_vec3s, depending on the coupling mode
    !> (up, down, same), and multiplied by the corresponding component of the electric field.
    !>
    !> Molecular-specific notes:
    !> - In molecular calculations, some (j,i) pairs can be coupled by more than one component of the
    !>   dipole operator, which means that we need to consider all three coupling modes for each (j,i)
    !>   combination.
    !> - Also, in molecular calculations, dipole operator can yield non-zero matrix elements even
    !>   for diagonal blocks, i.e. (j,j) /= 0. So, if i == j, the block master 'j' will actually send
    !>   to itself. This must be taken in account below to avoid MPI deadlock and is the reason for
    !>   the MPI_Issend - MPI_Recv - MPI_Wait sequence in Send_Recv_Master_Vecs_Ptp_Cmpt.
    !>
    !> \param my_vec          Segment of the wave function controlled by the current MPI process.
    !> \param my_vec1s        Storage for "up" segments needed by this block master.
    !> \param my_vec2s        Storage for "down" segments needed by this block master.
    !> \param my_vec3s        Storage for "same" segments needed by this block master.
    !> \param my_blocksize1s  Number of states in the "up" segment.
    !> \param my_blocksize2s  Number of states in the "down" segment.
    !> \param my_blocksize3s  Number of states in the "same" segment.
    !> \param n_1             On return, number of "up" segments received.
    !> \param n_2             On return, number of "down" segments received.
    !> \param n_3             On return, number of "same" segments received.
    !> \param field_real      Cartesian components of the electric field in this time step.
    !>
    SUBROUTINE send_recv_master_vecs_ptp(my_vec, my_vec1s, my_vec2s, my_vec3s, &
                                         my_blocksize1s, my_blocksize2s, my_blocksize3s, &
                                         n_1, n_2, n_3, field_real)

        USE global_data,        ONLY: zero
        USE coupling_rules,     ONLY: field_factors
        USE mpi_layer_lblocks,  ONLY: Lb_m_rank
        USE readhd,             ONLY: dipole_coupled, &
                                      downcoupling, &
                                      samecoupling, &
                                      upcoupling, &
                                      my_no_of_couplings, &
                                      i_couple_to

        IMPLICIT NONE

        REAL(wp),    INTENT(IN)  :: field_real(3, numsols)
        COMPLEX(wp), INTENT(IN)  :: my_vec(max_L_block_size, numsols)
        COMPLEX(wp), INTENT(OUT) :: my_vec1s(max_L_block_size, numsols, no_vec1_init), &
                                    my_vec2s(max_L_block_size, numsols, no_vec2_init), &
                                    my_vec3s(max_L_block_size, numsols, no_vec3_init)
        INTEGER, INTENT(OUT)     :: my_blocksize1s(no_vec1_init), &
                                    my_blocksize2s(no_vec2_init), &
                                    my_blocksize3s(no_vec3_init)
        INTEGER, INTENT(OUT)     :: n_1, n_2, n_3
        INTEGER                  :: i, j, k, l, cnt
        COMPLEX(wp)              :: fieldfactor(3, numsols)
        COMPLEX(wp)              :: my_up_field(numsols, 3)
        COMPLEX(wp)              :: my_down_field(numsols, 3)
        COMPLEX(wp)              :: my_same_field(numsols, 3)
        INTEGER, DIMENSION(50)   :: req_array
        INTEGER                  :: tmp_req, req_cnt, ierr, isol

        ! vec1 = UP
        ! vec2 = DOWN
        ! vec3 = SAME

        n_1 = 0; my_blocksize1s = 0
        n_2 = 0; my_blocksize2s = 0
        n_3 = 0; my_blocksize3s = 0
        my_up_field = zero
        my_down_field = zero
        my_same_field = zero

        req_cnt = 0
        req_array=0
        DO l = 1, my_no_of_couplings
            DO k = 1, my_no_of_couplings
                i = i_couple_to(l)
                j = i_couple_to(k)

                ! skip (j,i) coupling if the present rank is neither sender, nor receiver
                IF (Lb_m_rank + 1 /= i .AND. Lb_m_rank + 1 /= j) CYCLE

                cnt = dipole_coupled(j, i)

                ! calculate factors to multiply the dipole blocks with (differs for atomic/molecular mode)
                DO isol = 1, numsols
                    CALL field_factors(field_real(:, isol), j, i, fieldfactor(:, isol))
                END DO

                ! Avoid needless MPI communication when the field is zero ACB
                IF ( (IAND(cnt, upcoupling) /= 0) .AND. ANY(fieldfactor(1, :) /= zero) ) THEN
                    CALL send_recv_master_vecs_ptp_cmpt(fieldfactor(1, :), i, my_vec, j,&
                                                        n_1, my_vec1s, my_blocksize1s,&
                                                        no_vec1_init, tmp_req, my_up_field)
                    req_cnt = req_cnt + 1
                    req_array(req_cnt) = tmp_req
                END IF

                IF ( (IAND(cnt, downcoupling) /= 0) .AND. ANY(fieldfactor(2, :) /= zero) ) THEN
                    CALL send_recv_master_vecs_ptp_cmpt(fieldfactor(2, :), i, my_vec, j,&
                                                        n_2, my_vec2s, my_blocksize2s,&
                                                        no_vec2_init, tmp_req, my_down_field)
                    req_cnt = req_cnt + 1
                    req_array(req_cnt) = tmp_req
                END IF

                IF ( (IAND(cnt, samecoupling) /= 0) .AND. ANY(fieldfactor(3, :) /= zero) ) THEN
                    CALL send_recv_master_vecs_ptp_cmpt(fieldfactor(3, :), i, my_vec, j,&
                                                        n_3, my_vec3s, my_blocksize3s,& 
                                                        no_vec3_init, tmp_req, my_same_field)
                    req_cnt = req_cnt + 1
                    req_array(req_cnt) = tmp_req
                END IF

            END DO !l
        END DO !k

        CALL MPI_WAITALL(req_cnt, req_array(1:req_cnt), MPI_STATUSES_IGNORE, ierr)

        ! process the received data by multiplying the vector by the relevant field component
        CALL vector_field_multiplication(n_1, my_up_field, my_vec1s, no_vec1_init)
        CALL vector_field_multiplication(n_2, my_down_field, my_vec2s, no_vec2_init)
        CALL vector_field_multiplication(n_3, my_same_field, my_vec3s, no_vec3_init)

    END SUBROUTINE send_recv_master_vecs_ptp

!-----------------------------------------------------------------------

    !> \brief   Process wavefunction data by multiplying with the relevant field component
    !> \authors A Brown
    !> \date    2018
    !>
    !> The subroutine multiplies the received wavefunction vectors with the  relevant field components in
    !> preparation for multiplication with the dipole blocks.
    !>
    !> \param n             Number of wavefunction vectors to process
    !> \param my_field      Array of field components corresponding to wavefunction vectors
    !> \param my_vec        Array of wavefunction segments
    !> \param nvecs         Maximal dimension of \c my_vec
    !>
    SUBROUTINE vector_field_multiplication(n, my_field, my_vec, nvecs)
        INTEGER, INTENT(IN)        :: n
        COMPLEX(WP), INTENT(IN)    :: my_field(numsols, 3)
        INTEGER, INTENT(IN)        :: nvecs
        COMPLEX(WP), INTENT(INOUT) :: my_vec(max_L_block_size, numsols, nvecs)
        INTEGER                    :: ii, isol

        DO ii = 1, n
            DO isol = 1, numsols
                my_vec(:, isol, ii) = my_field(isol, ii) * my_vec(:, isol, ii)
            END DO
        END DO

    END SUBROUTINE vector_field_multiplication
!-----------------------------------------------------------------------

    SUBROUTINE order0_pes_sum_vecs(vecin, vecout)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(numrows, numsols)
        COMPLEX(wp), INTENT(OUT)    :: vecout(max_L_block_size, numsols)
        INTEGER                     :: ierr, isol

        vecout = (0.0_wp, 0.0_wp)

        DO isol = 1, numsols
            ! Collect all vectors of dim(numrows) and join into vector of dim(max_L_block_size)
            ! -- to get a vector for this symmetry block for this set of propagation orders
            CALL MPI_GATHERV(vecin(:, isol), numrows, MPI_DOUBLE_COMPLEX, vecout(:, isol), &
                            mv_counts, mv_disp, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
        END DO

        ! Bcast the result to rest of L block
        CALL MPI_BCAST(vecout, max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)

    END SUBROUTINE order0_pes_sum_vecs

!-----------------------------------------------------------------------

    SUBROUTINE get_global_pop_inner(vecin, tnorm)

        USE mpi_layer_lblocks,  ONLY: my_LML_block_id, &
                                      Lb_m_comm
        USE readhd,             ONLY: states_per_LML_block

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)    :: vecin(max_L_block_size, numsols)
        REAL(wp), INTENT(OUT)      :: tnorm(numsols)
        REAL(wp)                   :: norm(numsols)
        INTEGER                    :: ierr, i, isol
        INTEGER                    :: L_block_size

        L_block_size = states_per_LML_block(my_LML_block_id)

        DO isol = 1, numsols
            norm(isol) = 0.0_wp
            DO i = 1, L_block_size
                norm(isol) = norm(isol) + REAL(CONJG(vecin(i, isol))*vecin(i, isol))
            END DO
        END DO

        CALL MPI_REDUCE(norm, tnorm, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, 0, Lb_m_comm, ierr)

    END SUBROUTINE get_global_pop_inner

!-----------------------------------------------------------------------

    SUBROUTINE get_global_expec_Z_inner(my_local_var, global_var)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)  :: my_local_var
        COMPLEX(wp), INTENT(OUT) :: global_var
        INTEGER :: ierr

        CALL MPI_REDUCE(my_local_var, global_var, 1, &
                        MPI_DOUBLE_COMPLEX, MPI_SUM, 0, mpi_comm_region, ierr)

    END SUBROUTINE get_global_expec_Z_inner

!-----------------------------------------------------------------------

    SUBROUTINE sum_and_distribute_total_beta(vecin, a, b, tbeta)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(:, :)
        INTEGER,     INTENT(IN)     :: a, b
        REAL(wp),    INTENT(OUT)    :: tbeta(numsols)
        REAL(wp)                    :: beta(numsols)
        INTEGER                     :: ierr, isol
        COMPLEX(wp), EXTERNAL       :: ZDOTC

        DO isol = 1, numsols
            beta(isol) = ZDOTC(numrows, vecin(a:b, isol), 1, vecin(a:b, isol), 1)
        END DO

        CALL MPI_ALLREDUCE(beta, tbeta, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)

        tbeta = SQRT(tbeta)

    END SUBROUTINE sum_and_distribute_total_beta

!-----------------------------------------------------------------------

    SUBROUTINE gather_C_time_derivs(vecin, vecout)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(numrows)
        COMPLEX(wp), INTENT(OUT)    :: vecout(max_L_block_size)
        INTEGER                     :: ierr

        vecout = (0.0_wp, 0.0_wp)

        ! Collect all vectors of dim(numrows) and join into vector of dim(max_L_block_size)
        ! -- to get a vector for this symmetry block
        CALL MPI_GATHERV(vecin, numrows, MPI_DOUBLE_COMPLEX, vecout, &
                         mv_counts, mv_disp, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)

    END SUBROUTINE gather_C_time_derivs

!-----------------------------------------------------------------------

    SUBROUTINE allocate_my_vecs

        IMPLICIT NONE

        INTEGER :: err

        ALLOCATE (my_vec1s(max_L_block_size, numsols, no_vec1_init), &
                  my_vec2s(max_L_block_size, numsols, no_vec2_init), &
                  my_vec3s(max_L_block_size, numsols, no_vec3_init), &
                  my_blocksize1s(no_vec1_init), my_blocksize2s(no_vec2_init), my_blocksize3s(no_vec3_init), &
                  stat=err)

        CALL assert(err .EQ. 0, 'allocation error with vecs')

        my_vec1s = (0.0_wp, 0.0_wp)
        my_vec2s = (0.0_wp, 0.0_wp)
        my_vec3s = (0.0_wp, 0.0_wp)

        my_blocksize1s = 0
        my_blocksize2s = 0
        my_blocksize3s = 0

    END SUBROUTINE allocate_my_vecs

END MODULE live_communications
