! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Main program file for the RMT program.

PROGRAM tdse

    USE tdse_dependencies

    IMPLICIT NONE

    REAL(wp)   :: r_at_region_bndry
    INTEGER    :: number_channels
    REAL(wp)   :: Z_minus_N

    !-----------------------------------------------------------------------
    ! Variables for breaking integration up into stages, and for initializing
    ! Psi.
    !-----------------------------------------------------------------------
    INTEGER    :: timeindex
    INTEGER    :: start_of_run_timeindex

    REAL(wp)   :: field_evaluation_time
    REAL(wp)   :: iteration_start_time
    REAL(wp)   :: iteration_finish_time

    INTEGER    :: start_of_chkpt_timeindex
    INTEGER    :: end_of_chkpt_timeindex

    INTEGER    :: stage
    INTEGER    :: isol
    INTEGER    :: checkpoint_id

    REAL(wp), ALLOCATABLE :: current_field_strength(:,:)

    INTEGER    :: pop_index

    INTEGER    :: my_post

!                           ___      _ _
!                          |_ _|_ _ (_) |_
!                           | || ' \| |  _|
!                          |___|_||_|_|\__|

    CALL initialise_everything(r_at_region_bndry, Z_minus_N, number_channels, my_post, start_of_run_timeindex, stage)

    ALLOCATE (current_field_strength(3, no_of_field_confs))
    current_field_strength = 0.0_wp


!       __      __              ___                _   _
!       \ \    / /_ ___ _____  | __|__ _ _  _ __ _| |_(_)___ _ _
!        \ \/\/ / _` \ V / -_) | _|/ _` | || / _` |  _| / _ \ ' \
!         \_/\_/\__,_|\_/\___| |___\__, |\_,_\__,_|\__|_\___/_||_|
!
!
!             INTEGRATE WAVE EQUATION: d_Psi = -i * H * Psi

    DO checkpoint_id = 1, checkpts_per_run

        pop_index = data_1st - 1

        start_of_chkpt_timeindex = &
            start_of_run_timeindex + (checkpoint_id - 1)*steps_per_checkpt

        end_of_chkpt_timeindex = &
            start_of_chkpt_timeindex + steps_per_checkpt - 1

        DO timeindex = start_of_chkpt_timeindex, end_of_chkpt_timeindex

            iteration_start_time = (timeindex - timeindex_first)*delta_t
            iteration_finish_time = iteration_start_time + delta_t
            field_evaluation_time = iteration_start_time + 0.50_wp*delta_t

            DO isol = 1, no_of_field_confs
                current_field_strength(:, isol) = get_e_pulse(field_evaluation_time, isol)
            END DO

            CALL arnoldi_loop(number_channels, deltaR, Z_minus_N, current_field_strength, timeindex)

            CALL update_wavefunction_module(timeindex, iteration_start_time, iteration_finish_time, &
                                            current_field_strength, pop_index, my_post)

        END DO  ! TimeIndex loop

        CALL checkpoint_writes(stage, stage_first, pop_index, timeindex, Z_minus_N, r_at_region_bndry, max_L_block_size)

        stage = stage + 1

    END DO ! Checkpoint_ID

    CALL end_program

END PROGRAM tdse
