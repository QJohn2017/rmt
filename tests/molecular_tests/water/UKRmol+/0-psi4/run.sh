#!/bin/bash

# Generate MOLDEN file with the Gaussian molecular orbitals
# for the ionized water molecule. See the directory "outputs"
# for sample output files.

psi4 --input inputs/h2o+.inp --output outputs/h2o+.out
