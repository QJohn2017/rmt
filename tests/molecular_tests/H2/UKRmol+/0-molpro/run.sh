#!/bin/bash

# Generate MOLDEN file with the Gaussian molecular orbitals
# for the ionized hydrogen molecule. See the directory "outputs"
# for sample output files.

ln -s inputs/H2+.inp

molpro H2+.inp
