#!/bin/bash

# Calculate dipole transition moment between all inner-region CSFs across all symmetries.
# Then, use the utility "rmt_interface" to write RMT input data file "molecular_data".

echo "H₂: cdenprop (target)"

ln -s ../1-integrals/moints fort.17
ln -s ../2-inner/target-properties.dat fort.24
ln -s ../2-inner/scattering-ci-coeffs.dat fort.25
ln -s ../2-inner/target-ci-coeffs.dat fort.26

ln -s ../2-inner/scattering-configurations-1.dat fort.70
ln -s ../2-inner/scattering-configurations-2.dat fort.71
ln -s ../2-inner/scattering-configurations-3.dat fort.72
ln -s ../2-inner/scattering-configurations-4.dat fort.73
ln -s ../2-inner/scattering-configurations-5.dat fort.74
ln -s ../2-inner/scattering-configurations-6.dat fort.75
ln -s ../2-inner/scattering-configurations-7.dat fort.76
ln -s ../2-inner/scattering-configurations-8.dat fort.77

cdenprop_target < inputs/cdenprop.inp 1> cdenprop_target.out 2> cdenprop_target.err || exit

echo "H₂: RMT interface"

ln -s ../1-integrals/moints fort.22

rmt_interface < inputs/rmt_interface.inp 1> rmt_interface.out 2> rmt_interface.err || exit
